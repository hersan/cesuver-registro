<?php

namespace Tests\Feature;

use App\Entity\Evaluation;
use App\Entity\Place;
use App\Entity\User;
use test\Mockery\Fixtures\VoidMethod;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PlaceTest extends TestCase
{
    use DatabaseTransactions;

    public function test_show_all_places():void
    {
        $user = factory(User::class)->create();
        $places = Place::all();

        $this->actingAs($user)
            ->get('places')
            ->assertStatus(200)
            ->assertViewIs('place.index')
            ->assertViewHasAll(['places' => $places]);
    }

    public function test_create_a_new_place():void
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->post(route('places.store'), [
                'name' => 'test name of the place',
            ], ['HTTP_REFERER' => route('places.create')])
            ->assertRedirect(route('places.create'))
            ->assertSessionHas('status', 'La institución fue creada');

        $this->assertDatabaseHas('places', [
            'name' => 'test name of the place',
        ]);
    }

    public function test_update_existing_place():void
    {
        $user = factory(User::class)->create();
        $place = factory(Place::class)->create();

        $this->actingAs($user)
            ->put(route('places.update', $place), [
                'id' => $place->id,
                'name' => 'Other Name'
            ], ['HTTP_REFERER' => route('places.edit',$place)])
            ->assertRedirect(route('places.edit', $place))
            ->assertSessionHas('status', 'La institución fue actualizada')
        ;
        $this->assertDatabaseHas('places', [
            'id' => $place->id,
            'name' => 'Other Name'
        ]);
    }

    /**
     * @param $expected
     * @param $value
     * @dataProvider failInputs
     */
    public function test_validation_fails_when_it_try_to_create_a_place($expected, $value):void
    {
        $user = factory(User::class)->create();
        factory(Place::class)->create([
            'name' => 'Repeated name',
        ]);
        $this->actingAs($user)
            ->post(route('places.store'), [
                'name' => $value,
            ], ['HTTP_REFERER' => route('places.create')])
            ->assertRedirect(route('places.create'))
            ->assertSessionHasErrors($expected);

    }

    public function test_fails_when_the_place_has_relations(): void
    {
        $evaluation = factory(Evaluation::class)->create();
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->delete(route('places.destroy', $evaluation->place), [
                'id' => $evaluation->place->id,
            ], ['HTTP_REFERER' => route('places.index')])
            ->assertRedirect(route('places.index'))
            ->assertSessionHas('warning', 'No se puede borrar esta institución');

        $this->assertDatabaseHas('places', $evaluation->place->toArray());
    }

    public function test_delete_place(): void
    {
        $place = factory(Place::class)->create();
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->delete(route('places.destroy', $place), [
                'id' => $place->id,
            ], ['HTTP_REFERER' => route('places.index')])
            ->assertRedirect(route('places.index'))
            ->assertSessionHas('success', 'El elemento fue borrado');

        $this->assertDatabaseMissing('places', [
           'id' => $place->id
        ]);
    }

    public function failInputs() : array
    {
        return [
            ['name', null],
            ['name', ''],
            ['name', 1234],
            ['name', 'Repeated name']
        ];
    }
}
