<?php

namespace Tests\Feature;

use App\Entity\Evaluation;
use App\Entity\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ResultsControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_does_not_generate_a_report_if_there_are_no_open_evaluations()
    {
        $user = factory(User::class)->create();
        $user->evaluations()->save(factory(Evaluation::class)->create([
            'status' => 1,
        ]));

        $response = $this->actingAs($user)->get(route('applicant.results', $user));
        $response->assertViewIs('result.global_results');
    }
}
