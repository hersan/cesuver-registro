<?php

namespace Tests\Feature\Services;

use Tests\TestCase;
use App\Services\FloatToStringFormatter;

class FloatToStringFormatterTest extends TestCase
{
    /**
     * Test formatting a float number with multiple decimal places.
     */
    public function testFormatFloatWithMultipleDecimalPlaces()
    {
        $result = FloatToStringFormatter::formatFloat(123.456);
        $this->assertEquals('123.4', $result);
    }

    /**
     * Test formatting a float number with no decimal places.
     */
    public function testFormatFloatWithNoDecimalPlaces()
    {
        $result = FloatToStringFormatter::formatFloat(123);
        $this->assertEquals('123.0', $result);
    }

    /**
     * Test formatting a float number with a single decimal place.
     */
    public function testFormatFloatWithSingleDecimalPlace()
    {
        $result = FloatToStringFormatter::formatFloat(123.4);
        $this->assertEquals('123.4', $result);
    }

    /**
     * Test formatting a float number that ends with a zero in its decimals.
     */
    public function testFormatFloatWithTrailingZeroInDecimals()
    {
        $result = FloatToStringFormatter::formatFloat(123.40);
        $this->assertEquals('123.4', $result);
    }

    /**
     * Test formatting a negative float number with multiple decimal places.
     */
    public function testFormatFloatWithNegativeNumber()
    {
        $result = FloatToStringFormatter::formatFloat(-123.456);
        $this->assertEquals('-123.4', $result);
    }

    /**
     * Test formatting a number with very large decimals.
     */
    public function testFormatFloatWithLargeDecimals()
    {
        $result = FloatToStringFormatter::formatFloat(123.987654321);
        $this->assertEquals('123.9', $result);
    }
}