<?php

namespace Tests\Feature;

use App\Entity\Evaluation;
use App\Entity\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EvaluationTest extends TestCase
{
    use DatabaseTransactions;

    public function test_check_if_has_a_valid_evaluation_with_scores()
    {

        $user = factory(User::class)->create();
        $user->evaluations()->save(factory(Evaluation::class)->create([
            'status' => 1,
        ]));

        $this->assertEquals(0, Evaluation::hasScoresFor($user)->count());
    }

    public function test_users_without_evaluations_cant_have_scores()
    {
        $user = factory(User::class)->create();

        $this->assertEquals(0, Evaluation::hasScoresFor($user)->count());
    }
}
