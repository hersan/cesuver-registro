<?php

namespace Tests\Feature;

use App\Entity\Schedule;
use App\Entity\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ScheduleControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_show_all_dates(): void
    {
        $user = factory(User::class)->create();
        factory(Schedule::class, 5);
        $schedules = Schedule::all();

        $this->actingAs($user)
            ->get('schedules')
            ->assertStatus(200)
            ->assertViewIs('schedule.index')
            ->assertSeeText('Fechas de aplicación')
            ->assertViewHasAll(['schedules' => $schedules])
        ;
    }

    public function test_create_a_new_schedule(): void
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->post(route('schedules.store'), [
                'date_at' => Carbon::now()->toDateString(),
            ], ['HTTP_REFERER' => route('schedules.create')])
            ->assertRedirect(route('schedules.create'))
            ->assertSessionHas('status', 'La fecha fue agregada');
        ;
    }

    public function test_update_existing_schedule(): void
    {
        $user = factory(User::class)->create();
        $schedule = factory(Schedule::class)->create();

        $this->actingAs($user)
            ->put(route('schedules.update', $schedule), [
                'date_at' => Carbon::now()->toDateString(),
            ], ['HTTP_REFERER' => route('schedules.edit', $schedule)])
            ->assertRedirect(route('schedules.edit', $schedule))
            ->assertSessionHas('status', 'La fecha fue actualizada');
        ;
    }

    public function test_should_delete_schedule(): void
    {
        $user = factory(User::class)->create();
        $schedule = factory(Schedule::class)->create();

        $this->actingAs($user)
            ->delete(route('schedules.destroy', $schedule), [
            ], ['HTTP_REFERER' => route('schedules.index')])
            ->assertRedirect(route('schedules.index'))
            ->assertSessionHas('success', 'El elemento fue borrado')
        ;
    }

    /**
     * @dataProvider failInputs
     * @param $expected
     * @param $value
     */
    public function test_validation_fails_when_this_tries_to_create_a_schedule($expected, $value): void
    {
        $user = factory(User::class)->create();
        //for unique rule
        factory(Schedule::class)->create([
            'date_at' => Carbon::now()->toDateString()
        ]);

        $this->actingAs($user)
            ->post(route('schedules.store'), [
                'date_at' => $value,
            ], ['HTTP_REFERER' => route('schedules.create')])
            ->assertRedirect(route('schedules.create'))
            ->assertSessionHasErrors($expected)
        ;
    }

    public function failInputs(): array
    {
        return [
            ['date_at', ''],
            ['date_at', 'text'],
            ['date_at', 123456],
            ['date_at', null],
            ['date_at', '2018/12/01'],
            ['date_at', '01-02-2018'],
            ['date_at', '01/02/2018'],
            ['date_at', Carbon::now()->subDay()->toDateString()],
            ['date_at', Carbon::now()->subMonth()->toDateString()],
            ['date_at', Carbon::now()->subYear()->toDateString()],
            ['date_at', Carbon::now()->toDateString()],
        ];
    }
}
