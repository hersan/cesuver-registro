<?php

namespace Tests\Unit;

use App\Services\Schedule;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ScheduleServiceTest extends TestCase
{
    use DatabaseTransactions;

    protected $dates = [
        5 => '2019-03-23',
        6 => '2019-03-24',
        7 => '2019-04-20',
        8 => '2019-04-21',
        9 => '2019-05-18',
        10 => '2019-05-19',
        11 => '2019-06-22',
        12 => '2019-06-23',
        13 => '2019-07-20',
        14 => '2019-07-21',
        15 => '2019-08-17',
        16 => '2019-08-18',
        17 => '2019-09-21',
        18 => '2019-09-22',
        19 => '2019-10-19',
        20 => '2019-10-20',
        21 => '2019-11-16',
        22 => '2019-11-17',
        23 => '2019-12-07',
        24 => '2019-12-08',
    ];

    protected $wrongDates = [
        1 => '2018-03-24',
        2 => '2018-06-23',
        3 => '2018-09-22',
        4 => '2018-12-15',
    ];

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function test_can_get_date_by_id(): void
    {
        $scheduleModel = factory(\App\Entity\Schedule::class)->create([
            'date_at' => Carbon::now()->toDateString(),
        ]);
        $schedule = new Schedule();
        $this->assertEquals(Carbon::now()->toDateString(), $schedule->getByKey($scheduleModel->id));
    }

    public function test_can_format_dates_to_spanish(): void
    {
        $schedule = new Schedule();

        $this->assertEquals($this->translateToSpanish(Carbon::now()), $schedule->format(Carbon::now()));
    }

    public function test_can_get_all_dates_in_spanish(): void
    {
        collect($this->dates)->each(function ($item){
            factory(\App\Entity\Schedule::class)->create([
                'date_at' => $item
            ]);
        });

        $dates = \App\Entity\Schedule::all();

        $schedule = new Schedule();

        $this->assertEquals(
            $this->translateToSpanish($dates),
            $schedule->get()
        );
    }

    public function translateToSpanish($date)
    {
        $IntlDateFormatter = new \IntlDateFormatter(
            'es_ES',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE
        );

        if ($date instanceof Collection) {
            return  $date->mapWithKeys(function ($item) use($IntlDateFormatter){
                return [$item->id => $IntlDateFormatter->format(Carbon::createFromFormat('Y-m-d', $item->date_at))];
            });
        }

        if (is_array($date)) {
            return  collect($date)->map(function ($item) use($IntlDateFormatter){
                return $IntlDateFormatter->format($item->date_at);
            });
        }

        return $IntlDateFormatter->format($date);
    }
}
