<?php

namespace Tests\Unit;

use App\Support\Schedule;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ScheduleTest extends TestCase
{
    private $dates = [
        5 => '23 de marzo de 2019',
        6 => '24 de marzo de 2019',
        7 => '20 de abril de 2019',
        8 => '21 de abril de 2019',
        9 => '18 de mayo de 2019',
        10 => '19 de mayo de 2019',
        11 => '22 de junio de 2019',
        12 => '23 de junio de 2019',
        13 => '20 de julio de 2019',
        14 => '21 de julio de 2019',
        15 => '17 de agosto de 2019',
        16 => '18 de agosto de 2019',
        17 => '21 de septiembre de 2019',
        18 => '22 de septiembre de 2019',
        19 => '19 de octubre de 2019',
        20 => '20 de octubre de 2019',
        21 => '16 de noviembre de 2019',
        22 => '17 de noviembre de 2019',
        23 => '7 de diciembre de 2019',
        24 => '8 de diciembre de 2019',
    ];

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $schedule = new Schedule();

        $this->assertEquals('2019', strstr($schedule->get()[10], '2019'));
    }

    public function test_can_show_only_dates_of_the_current_year()
    {
        $schedule = new Schedule();

        $this->assertEquals($this->getDatesByCurrentYear(), $schedule->get()->toArray());
    }

    public function getDatesByCurrentYear($year = null)
    {
        if (is_null($year)) {
            $year = Carbon::now()->year;
        }

        return array_filter($this->dates, function ($item) use ($year){
            return strpos($item, (string)$year) !== false;
        });
    }
}
