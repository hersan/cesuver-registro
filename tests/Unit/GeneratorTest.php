<?php

namespace Tests\Unit;

use App\Entity\Generator;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GeneratorTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function test_generate_correct_sequence()
    {
        $generator = factory(Generator::class)->create();
        $this->assertEquals('2019-1', $generator->code(Carbon::now()));
    }

    public function test_should_not_remain_previos_sequence()
    {
        $generator = factory(Generator::class)->create();
        $generator->code(Carbon::now());
        $generator->code(Carbon::now());
        $generator->code(Carbon::now());

        $generator = Generator::find($generator->id);
        $this->assertEquals('2020-1', $generator->code(Carbon::now()->addYear()));
    }

    public function test_third_call_get_correct_year_and_sequence()
    {
        $generator = factory(Generator::class)->create([]);

        $generator->code(Carbon::now());

        $generator1 = Generator::find($generator->id);

        $generator1->code(Carbon::now());
        //dd($generator1);

        $this->assertEquals('2019-3', $generator1->code(Carbon::now()));
    }

    public function test_this_reset_the_sequence_when_the_year_change(): void
    {
        $generator = factory(Generator::class)->create([
            'code_year' => Carbon::now()->subYear(1)->year,
            'sequence' => 520
        ]);

        $generator->code(Carbon::now());

        $generator1 = Generator::find($generator->id);

        $generator1->code(Carbon::now());
        $generator1->code(Carbon::now());

        $this->assertEquals('2019-4', $generator1->code(Carbon::now()));
    }

    public function test_it_get_the_next_correct_sequence(): void
    {
        $generator = factory(Generator::class)->create([
            'code_year' => Carbon::now()->year,
            'sequence' => 10
        ]);

        $generator1 = Generator::find($generator->id);

        $generator1->code(Carbon::now());
        $generator1->code(Carbon::now());

        $this->assertEquals('2019-13', $generator1->code(Carbon::now()));
    }
}
