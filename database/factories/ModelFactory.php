<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Entity\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        //'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = 'secret',
        'confirmed' => 1,
        'remember_token' => str_random(10),
        'status' => $faker->randomElement([0,1]),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Entity\Subject::class, function(Faker\Generator $faker){
   return [
       'name' => $faker->word,
   ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Entity\Generator::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'sequence' => 0,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Entity\Place::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->word,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Entity\Evaluation::class, function(Faker\Generator $faker){
    return [
        'code' => $faker->uuid,
        'scheduled_date' => $faker->date(),
        'comments' => $faker->paragraph,
        'place_id' => function() {
            return factory(\App\Entity\Place::class)->create();
        },
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Entity\Schedule::class, function (Faker\Generator $faker) {

    return [
        'date_at' => $faker->dateTimeBetween('now', '+1 years'),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Entity\Address::class, function (Faker\Generator $faker) {

    return [
        'street' => $faker->streetAddress,
        'neighborhood' => $faker->word,
        'postal_code' => $faker->postcode,
        'state' => $faker->state,
        'city' => $faker->city,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Entity\Identity::class, function (Faker\Generator $faker) {

    return [
        'first_name' => $faker->firstName,
        'second_name' => $faker->lastName,
        'last_name' => $faker->lastName,
        'sex' => $faker->randomElement(['H','M']),
        'birthday' => $faker->dateTimeBetween('-10 years'),
        'curp' => $faker->ean8,
        'curp_file' => $faker->word,
        'identity_card' => $faker->word,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Entity\Profile::class, function (Faker\Generator $faker) {

    return [
        'degree_in' => 1,
        'proof_of_sufficiency' => true,
        'proof_of_sufficiency_file' => $faker->word,
        'curriculum' => $faker->word,
        'portfolio_of_evidence' => $faker->word,
        'descriptive_memory' => $faker->word,
        'subject_id' => $faker->randomElement([1,5]),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Entity\Telephone::class, function (Faker\Generator $faker) {

    return [
        'number' => $faker->phoneNumber,
        'telephone_type' => $faker->randomElement([1,2]),
    ];
});

