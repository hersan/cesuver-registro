<?php

use Illuminate\Database\Seeder;

class SubjectSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Entity\Subject::class)->create([
            'name' => 'Lic. En Mercadotecnia',
        ]);
        factory(App\Entity\Subject::class)->create([
            'name' => 'Lic. En Pedagogía',
        ]);
        factory(App\Entity\Subject::class)->create([
            'name' => 'Lic. En Administración',
        ]);
        factory(App\Entity\Subject::class)->create([
            'name' => 'Lic. En Comercio y Negocios Internacionales',
        ]);

        factory(App\Entity\Subject::class)->create([
            'name' => 'Materia unica',
            'scale' => 2,
        ]);
    }
}
