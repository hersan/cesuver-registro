<?php

use Illuminate\Database\Seeder;

class GeneratorSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Entity\Generator::class)->create();
    }
}
