<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Entity\User::class)->create([
            'email' => 'admin@cesuver.edu.mx',
            'password' => 'c3suv3r',
            'confirmed' => 1,
            'remember_token' => str_random(10),
            'is_admin' => true,
            'status' => 0,
        ]);

        factory(\App\Entity\User::class, 10)->create()->each(function ( $user){
            $user->identities()->save(factory(App\Entity\Identity::class)->create());
            $user->telephones()->save(factory(App\Entity\Telephone::class)->create());
            $user->addresses()->save(factory(App\Entity\Address::class)->create());
            $user->profiles()->save(factory(App\Entity\Profile::class)->create());
        });
    }
}
