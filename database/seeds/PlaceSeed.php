<?php

use Illuminate\Database\Seeder;

class PlaceSeed extends Seeder
{
    private $places = [
        'CESUVER VERACRUZ',
        'COLES XALAPA',
        'CONAIP CIUDAD DE MÉXICO',
        'CONAIP QUERÉTARO',
        'CONAIP CUERNAVACA',
        'EXPANSIÓN CIUDAD DE MÉXICO',
        'EXPANSIÓN GUADALAJARA',
        'MID CAPACITADORA',
        'UCC CIUDAD DE MÉXICO',
        'UNIVERSIDAD DEL ATLÁNTICO',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect($this->places)->each(function($item){
            factory(\App\Entity\Place::class)->create(['name' => $item]);
        });
    }
}
