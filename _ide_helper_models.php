<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Entity{
/**
 * App\Entity\Address
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $street
 * @property string $neighborhood
 * @property string $postal_code
 * @property string $state
 * @property string $city
 * @property string|null $references
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereNeighborhood($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereReferences($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereUserId($value)
 * @mixin \Eloquent
 */
	class Address extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\AspectsEvaluated
 *
 * @property int $id
 * @property int|null $evaluation_id
 * @property string $aspect
 * @property int $score
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereAspect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereEvaluationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class AspectsEvaluated extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Document
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $name
 * @property string|null $mime_type
 * @property int|null $document_type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereMimeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereUserId($value)
 * @mixin \Eloquent
 */
	class Document extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Evaluation
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $code
 * @property \Carbon\Carbon $scheduled_date
 * @property string|null $comments
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $place_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\AspectsEvaluated[] $aspects
 * @property-read \App\Entity\Place|null $place
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\EvaluationScore[] $scores
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation wherePlaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereScheduledDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereUserId($value)
 * @mixin \Eloquent
 */
	class Evaluation extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\EvaluationScore
 *
 * @property int $id
 * @property int|null $evaluation_id
 * @property int $evaluation_type
 * @property int $score
 * @property float|null $calculate_score
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereCalculateScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereEvaluationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereEvaluationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class EvaluationScore extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Generator
 *
 * @property int $id
 * @property int $sequence
 * @property string|null $code_year
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Generator whereCodeYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Generator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Generator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Generator whereSequence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Generator whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Generator extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Identity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $first_name
 * @property string|null $second_name
 * @property string|null $last_name
 * @property string|null $sex
 * @property string|null $birthday
 * @property string|null $curp
 * @property string|null $curp_file
 * @property string|null $identity_card
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $full_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereCurp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereCurpFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereIdentityCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereSecondName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereUserId($value)
 * @mixin \Eloquent
 */
	class Identity extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Place
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Evaluation[] $evaluations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Place whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Place whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Place whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Place whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Place extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Profile
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $degree_in
 * @property int|null $proof_of_sufficiency
 * @property string|null $proof_of_sufficiency_file
 * @property string|null $curriculum
 * @property string|null $portfolio_of_evidence
 * @property string|null $descriptive_memory
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $subject_id
 * @property-read \App\Entity\Subject|null $subject
 * @property-read \App\Entity\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereCurriculum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereDegreeIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereDescriptiveMemory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile wherePortfolioOfEvidence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereProofOfSufficiency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereProofOfSufficiencyFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereUserId($value)
 * @mixin \Eloquent
 */
	class Profile extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Subject
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Profile[] $profiles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subject whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subject whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Subject extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Telephone
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $number
 * @property int $telephone_type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereTelephoneType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereUserId($value)
 * @mixin \Eloquent
 */
	class Telephone extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\User
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $email
 * @property string $password
 * @property int $confirmed
 * @property string|null $confirmation_code
 * @property bool $is_admin
 * @property int $status
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Entity\Address $addresses
 * @property-read \App\Entity\Evaluation $evaluation
 * @property-read \App\Entity\Identity $identities
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Entity\Profile $profiles
 * @property-read \App\Entity\Subject $subject
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Telephone[] $telephones
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereConfirmationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

