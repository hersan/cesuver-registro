<?php
/**
 * Current File: ScheduleInterface.php
 * Project: cesuver-registro
 */

namespace App\Contracts;

interface ScheduleInterface
{
    public function get();

    public function getByKey($key);

    public function format($date);
}