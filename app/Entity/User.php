<?php

namespace App\Entity;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Entity\User
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $email
 * @property string $password
 * @property int $confirmed
 * @property string|null $confirmation_code
 * @property bool $is_admin
 * @property int $status
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Entity\Address $addresses
 * @property-read \App\Entity\Evaluation $evaluation
 * @property-read \App\Entity\Identity $identities
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Entity\Profile $profiles
 * @property-read \App\Entity\Subject $subject
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Telephone[] $telephones
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereConfirmationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereUpdatedAt($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    const PROGRAMMED  = 1;

    const THEORETICAL = 2;

    const PRACTICAL   = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'confirmation_code','confirmed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function profiles()
    {
        return $this->hasOne(Profile::class);
    }

    public function addresses()
    {
        return $this->hasOne(Address::class);
    }

    public function identities()
    {
        return $this->hasOne(Identity::class);
    }

    public function telephones()
    {
        return $this->hasMany(Telephone::class);
    }

    public function evaluations()
    {
        return $this->hasMany(Evaluation::class);
    }

    public function addProfile(Profile $profile)
    {
        $this->profiles()->save($profile);
    }

    public function addIdentity(Identity $identity)
    {
        $this->identities()->save($identity);
    }

    public function addTelephone(Telephone $telephone)
    {
        $this->telephones()->save($telephone);
    }

    public function addAddress(Address $address)
    {
        $this->addresses()->save($address);
    }

    public function isAdmin()
    {
        return $this->is_admin == 1;
    }
}
