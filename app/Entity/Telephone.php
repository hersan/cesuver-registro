<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Telephone
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $number
 * @property int $telephone_type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereTelephoneType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Telephone whereUserId($value)
 * @mixin \Eloquent
 */
class Telephone extends Model
{
    protected $fillable = [
        'number',
        'telephone_type',
    ];

}
