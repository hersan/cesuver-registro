<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Generator
 *
 * @property int $id
 * @property int $sequence
 * @property string|null $code_year
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Generator whereCodeYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Generator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Generator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Generator whereSequence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Generator whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Generator extends Model
{
    /**
     * @param Carbon $current
     * @return string
     */
    public function code(Carbon $current)
    {
        if ($this->isYearChange($current)) {
            $this->resetSequence();
        }

        $this->nextSequence($current);

        return sprintf('%s-%s', $this->code_year, $this->sequence);
    }

    /**
     * @param $current
     * @return bool
     */
    public function isYearChange($current)
    {
        return $this->whereCodeYear($current->year)->get()->isEmpty();
    }

    /**
     * @return void
     */
    public function resetSequence()
    {
        $this->sequence = 0;
        $this->save();
    }

    /**
     * @param $current
     * @return void
     */
    public function nextSequence($current)
    {
        $this->increment('sequence');
        $this->code_year = $current->year;
        $this->save();
    }
}
