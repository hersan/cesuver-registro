<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Address
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $street
 * @property string $neighborhood
 * @property string $postal_code
 * @property string $state
 * @property string $city
 * @property string|null $references
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereNeighborhood($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereReferences($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Address whereUserId($value)
 * @mixin \Eloquent
 */
class Address extends Model
{
    protected $fillable = [
        'street',
        'neighborhood',
        'postal_code',
        'postal_code',
        'state',
        'city',
        'references',
    ];
}
