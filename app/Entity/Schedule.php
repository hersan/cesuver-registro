<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = ['date_at','type'];
}
