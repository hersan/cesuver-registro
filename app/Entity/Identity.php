<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

/**
 * App\Entity\Identity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $first_name
 * @property string|null $second_name
 * @property string|null $last_name
 * @property string|null $sex
 * @property string|null $birthday
 * @property string|null $curp
 * @property string|null $curp_file
 * @property string|null $identity_card
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $full_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereCurp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereCurpFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereIdentityCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereSecondName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Identity whereUserId($value)
 * @mixin \Eloquent
 */
class Identity extends Model
{
    protected $fillable = [
        'first_name',
        'second_name',
        'last_name',
        'sex',
        'birthday',
        'curp',
    ];

    protected $dates = [
        'birthday',
    ];

    public function getFullNameAttribute()
    {
        return sprintf('%s %s %s', $this->first_name, $this->second_name, $this->last_name);
    }

    public function addDocument($type, Request $request)
    {
        if ($request->hasFile($type)) {
            $path = $request->{$type}->store(sprintf('documents/%s', $request->user()->id));
            $this->{$type} = $path;
            $this->save();
        }
    }

    public function addDocumentsFromRequest(Request $request)
    {
        collect($request->only(config('entity.identity.files')))->each(function ($file, $field) use ($request){
            /** @var UploadedFile $file*/
            if ($request->hasFile($field)) {
                $this->{$field} = $file->store(sprintf('documents/%s', $request->user()->id));
            }
        });

        $this->save();
    }
}
