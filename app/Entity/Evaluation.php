<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Evaluation
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $code
 * @property \Carbon\Carbon $scheduled_date
 * @property string|null $comments
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $place_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\AspectsEvaluated[] $aspects
 * @property-read \App\Entity\Place|null $place
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\EvaluationScore[] $scores
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation wherePlaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereScheduledDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Evaluation whereUserId($value)
 * @mixin \Eloquent
 */
class Evaluation extends Model
{
    const OPEN = 1;

    const CLOSED = 2;

    const APPLIED_THEORETICAL = 3;

    const PRACTICAL_APPLIED = 4;

    const EVALUATION_COMPLETED = 5;

    const THEORETICAL_EVALUATION = 1;

    const PRACTICAL_EVALUATION = 2;

    private $statuses = [
        self::OPEN => 'Abierta',
        self::CLOSED => 'Cerrada',
        self::APPLIED_THEORETICAL => 'Teórico Aplicado',
        self::PRACTICAL_APPLIED => 'Practico Aplicado',
        self::EVALUATION_COMPLETED => 'Evaluación Completa'
    ];

    protected $fillable = [
        'code', 'scheduled_date','comments', 'practical_date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'scheduled_date',
        'practical_date',
        'practical_evaluation_date',
    ];

    public function theoreticalScore()
    {
        return $this->hasMany(EvaluationScore::class)->whereEvaluationType(1);
    }

    public function practicalScore()
    {
        return $this->hasMany(EvaluationScore::class)->whereEvaluationType(2);
    }

    public function scores()
    {
        return $this->hasMany(EvaluationScore::class);
    }

    public function aspects()
    {
        return $this->hasMany(AspectsEvaluated::class);
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getStatusAttribute($value)
    {
        return $this->statuses[$value];
    }

    public function isOpen()
    {
        return $this->status === $this->statuses[Evaluation::OPEN];
    }

    public function isCompleted()
    {
        return $this->status === $this->statuses[Evaluation::EVALUATION_COMPLETED];
    }

    public function scopeHasScoresFor($query, User $user)
    {
        $query->whereStatus(Evaluation::OPEN)
                    ->whereUserId($user->id)
                    ->has('scores');
    }

    public function scopeHasAspectsFor($query, User $user)
    {
        $query->whereStatus(Evaluation::OPEN)
            ->whereUserId($user->id)
            ->has('aspects');
    }


}
