<?php

namespace App\Entity;

use App\Support\Calculator;
use App\Support\EvaluationType;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\EvaluationScore
 *
 * @property int $id
 * @property int|null $evaluation_id
 * @property int $evaluation_type
 * @property int $score
 * @property float|null $calculate_score
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereCalculateScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereEvaluationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereEvaluationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\EvaluationScore whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EvaluationScore extends Model
{
    protected $fillable = [
        'evaluation_type',
        'score',
        'calculate_score'
    ];

    /*public function setCalculateScoreAttribute($value)
    {
        $this->attributes['calculate_score'] = Calculator::score($value);
    }*/

    public function getEvaluationTypeAttribute()
    {
        return EvaluationType::getByKey($this->attributes['evaluation_type']);
    }
}
