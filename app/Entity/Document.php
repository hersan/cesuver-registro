<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Document
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $name
 * @property string|null $mime_type
 * @property int|null $document_type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereMimeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Document whereUserId($value)
 * @mixin \Eloquent
 */
class Document extends Model
{
    //
}
