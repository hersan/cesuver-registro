<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Place
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Evaluation[] $evaluations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Place whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Place whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Place whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Place whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Place extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluations()
    {
        return $this->hasMany(Evaluation::class);
    }

    public function canDeleted()
    {
        if (!$this->evaluations->count() ) {
            $this->delete();
        } else {
            throw new \Exception('No se puede borrar esta institución');
        }

    }
}
