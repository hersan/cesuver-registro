<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\AspectsEvaluated
 *
 * @property int $id
 * @property int|null $evaluation_id
 * @property string $aspect
 * @property int $score
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereAspect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereEvaluationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\AspectsEvaluated whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AspectsEvaluated extends Model
{
    protected $fillable = [
        'aspect',
        'score',
    ];

    protected $table = 'aspects_evaluated';
}
