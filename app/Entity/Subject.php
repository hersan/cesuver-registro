<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Subject
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Profile[] $profiles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subject whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Subject whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Subject extends Model
{
    protected $fillable = [
        'name',
        'scale'
    ];

    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }

    public function canDeleted()
    {
        if (!$this->profiles->count() ) {
            $this->delete();
        } else {
            throw new \Exception('No se puede borrar esta Licenciaura');
        }

    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
