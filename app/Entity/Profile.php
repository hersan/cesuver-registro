<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

/**
 * App\Entity\Profile
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $degree_in
 * @property int|null $proof_of_sufficiency
 * @property string|null $proof_of_sufficiency_file
 * @property string|null $curriculum
 * @property string|null $portfolio_of_evidence
 * @property string|null $descriptive_memory
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $subject_id
 * @property-read \App\Entity\Subject|null $subject
 * @property-read \App\Entity\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereCurriculum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereDegreeIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereDescriptiveMemory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile wherePortfolioOfEvidence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereProofOfSufficiency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereProofOfSufficiencyFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Profile whereUserId($value)
 * @mixin \Eloquent
 */
class Profile extends Model
{
    protected $fillable = [
        'degree_in',
    ];

    public function addDocument($type, Request $request)
    {
        if ($request->hasFile($type)) {
            $path = $request->{$type}->store(sprintf('documents/%s', $request->user()->id));
            $this->{$type} = $path;
            $this->save();
        }
    }

    public function addDocumentsFromRequest(Request $request)
    {
        collect($request->only(config('entity.profile.files')))->each(function ($file, $field) use ($request){
            /** @var UploadedFile $file*/
            if ($file instanceof UploadedFile) {
                $this->{$field} = $file->store(sprintf('documents/%s', $request->user()->id));
            }
        });

        $this->save();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    /**
     * @return bool
     */
    public function hasDescriptiveMemory()
    {
        return !empty($this->descriptive_memory);
    }

    public function hasCaseStudy()
    {
        return !empty($this->case_study);
    }
}
