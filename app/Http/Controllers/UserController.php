<?php

namespace App\Http\Controllers;

use App\Entity\Subject;
use App\Entity\Telephone;
use App\Entity\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::with(['identities', 'evaluations'])->where('is_admin', false)->get();

        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\User  $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        $subjects = Subject::all()->pluck('name','id');

        return view('user.edit', compact('user', 'subjects'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, $this->useRulesAccordingToTheRequest($request));

        $subject = Subject::find($request->degree_in);

        //dd($subject);

        try {
            DB::beginTransaction();

            $user->identities()->update(
                $request->only(config('entity.identity.fields'))
            );

            $user->addresses()->update(
                $request->only(config('entity.address.fields'))
            );

            collect($request->only([
                'telephones', 'telephone_type'
            ]))->transpose()->map(function($telephonesData){
                return new Telephone([
                    'number' => $telephonesData[0],
                    'telephone_type' => $telephonesData[1],
                ]);
            })->each(function ($telephone) use($user){
                $user->telephones()->update($telephone->toArray());
            });

            $user->profiles()->update(
                $request->only(config('entity.profile.fields'))
            );

            $user->identities->addDocumentsFromRequest($request);

            $user->profiles->addDocumentsFromRequest($request);

            $user->profiles->subject_id = $subject->id;
            $user->profiles->save();

            $user->save();

            DB::commit();
            // do your database transaction here
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return back()->with('status', 'Se presento un error en la consulta.');
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('status', 'Se presento un error.');
        }

        return back()->with('status', 'Su información se agrego al sistema: ¡Gracias!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    protected function downloadFile($src){
        if(is_file($src)){
            $finfo=finfo_open(FILEINFO_MIME_TYPE);
            $content_type= finfo_file($finfo,$src);
            finfo_close($finfo);
            $file_name = basename($src).PHP_EOL;
            $size = filesize($src);
            header("Content-Type: $content_type");
            header("Content-Disposition: attachment; filename=$file_name");
            header("Content-Transfer-Encoding: binary");
            header("Content-Lenght: $size");
            readfile($src);
            return true;
        } else{
            return false;
        }
    }

    public function download(){
        if (!$this->downloadFile(app_path()."/files/memoria_descriptiva.pdf")){
            return redirect()->back();
        }
    }

    public function useRulesAccordingToTheRequest(Request $request) {
        if ($request->option == 1) {
            return [
                'first_name' => 'required|string|min:3',
                'second_name' => 'required|string|min:3',
                //'last_name' => 'alpha',
                'sex' => 'required|numeric',
                'birthday' => 'required|date|date_format:Y-m-d|before:' . Carbon::now()->toDateString(),
                'curp' => 'required|alpha_num',
                'curp_file' => 'nullable|mimes:pdf',
                'identity_card' => 'nullable|mimes:pdf',
                'street' => 'required',
                'neighborhood' => 'required',
                'postal_code' => 'required|numeric|digits:5',
                'state' => 'required|string',
                'city' => 'required|string',
                'telephones.*' => 'required|numeric|digits:10',
                'telephone_type.*' => 'required|numeric',
                'degree_in' => 'required|string',
                'option' => 'required',
                'proof_of_sufficiency_file' => 'nullable|mimes:pdf',
                //'terms' => 'required|boolean',
            ];
        } else {
            return [
                'first_name' => 'required|string|min:3',
                'second_name' => 'required|string|min:3',
                //'last_name' => 'alpha',
                'sex' => 'required|numeric',
                'birthday' => 'required|date|date_format:Y-m-d|before:' . Carbon::now()->toDateString(),
                'curp' => 'required|alpha_num',
                'curp_file' => 'nullable|mimes:pdf',
                'identity_card' => 'nullable|mimes:pdf',
                'street' => 'required',
                'neighborhood' => 'required',
                'postal_code' => 'required|numeric|digits:5',
                'state' => 'required|string',
                'city' => 'required|string',
                'telephones.*' => 'required|numeric|digits:10',
                'telephone_type.*' => 'required|numeric',
                'degree_in' => 'required|string',
                'option' => 'required',
                'curriculum' => 'nullable|required|mimes:pdf',
                'portfolio_of_evidence' => 'nullable|required|mimes:pdf',
                //'terms' => 'required|boolean',
            ];
        }
    }
}
