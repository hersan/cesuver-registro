<?php

namespace App\Http\Controllers;

use App\Entity\User;
use App\Entity\Evaluation;
use App\Mail\GlobalReport;
use Illuminate\Http\Request;
use Mail;

class ResultsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, User $user)
    {
        if (! auth()->user()->isAdmin()) {
            return redirect()->route('home');
        }

        $evaluations = Evaluation::query()
            ->where('status',1)
            ->where('user_id',$user->id)
            ->with('scores')
            ->orderBy('created_at','desc')
            //->has('scores')
            ->first();

        if (!is_null($evaluations) && !$evaluations->scores->isEmpty()) {
            return $this->generateReport($user, 1);
        }

        $evaluations = Evaluation::query()
            ->where('status',5)
            ->where('user_id',$user->id)
            ->first();

        if ($evaluations != null) {
            return $this->generateReport($user, 5);
        }

        return view('result.global_results', [
            'evaluations' => $evaluations,
            'user' => $user,
            'total_score' => 0,
            'total_calculate_score' => 0,
            'competitions' => [],
            'scale'
        ]);
    }

    public function resultTheoretical(Request $request, User $user)
    {

    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function generateReport(User $user, $status)
    {
        $user->load('identities', 'profiles.subject');

        if($user->profiles->subject->scale != 2) {
            $scales = 'evaluation.scale';
            $max = config('evaluation.score');
        } else {
            $scales = 'evaluation.new_global_scale';
            $max = config('evaluation.max_new_practical_scale');
        }

        $evaluations = Evaluation::with(['scores','aspects'])
                        ->whereStatus($status)
                        ->whereUserId($user->id)->first();

        $total_score = $evaluations->scores->pluck('score')->sum();
        $total_calculate_score = $evaluations->scores->pluck('calculate_score')->sum() / 2;
        $competitions = collect(config($scales))->map(function ($scale) use ($total_score) {
            if ($total_score >= $scale['from'] && $total_score <= $scale['to']) {
                return $scale['value'];
            }
        })->filter()->values()->get(0);

        return view('result.global_results', compact('evaluations', 'total_score', 'total_calculate_score', 'competitions', 'user','scales'));
    }
}
