<?php

namespace App\Http\Controllers;

use App\Entity\Evaluation;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function change(Request $request)
    {

        $evaluation = Evaluation::find($request->id);

        if ($evaluation->isOpen() || $evaluation->isCompleted()) {
            $evaluation->status = Evaluation::CLOSED;
            $evaluation->save();
            return response()->json([
                'status' => true,
                'reason' => 'Evaluación cerrada con exito'
            ], 200);
        }

        return response()->json([
            'status' => false,
            'reason' => 'Esta evaluación ya esta cerrada'
        ], 200);
    }
}
