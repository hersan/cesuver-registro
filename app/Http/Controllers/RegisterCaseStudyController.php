<?php

namespace App\Http\Controllers;

use App\Entity\User;
use App\Entity\Profile;
use Illuminate\Http\Request;
use App\Mail\CaseStudyReception;
use Illuminate\Support\Facades\Mail;
use App\Mail\DescriptiveMemoryReception;

class RegisterCaseStudyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('responsive_case_study');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'case_study' => 'required|mimes:pdf',
        ]);

        $profiles = User::whereId(auth()->id())->has('profiles')->first();

        if($profiles == null) {
            return back()->with('warning', 'Necesita registrar una solicitud');
        }

        $evaluation = User::whereId(auth()->id())->has('evaluations')->first();

        if($evaluation == null) {
            return back()->with('warning', 'Necesita tener un evaluación programada');
        }

        $profile = Profile::whereUserId(auth()->id())->first();

        if ($profile->hasDescriptiveMemory()) {
            return back()->with(
                'warning',
                'No se puede guradar si ya tienes una memoria descriptiva'
            );
        }

        $profile->addDocument('case_study', $request);

        $user = auth()->user();
        $user->load('evaluations','identities', 'profiles');

        Mail::to(auth()->user())->send(new CaseStudyReception($user));

        return back()->with('status', 'Su responsiva de caso práctico se agrego al sistema: ¡Gracias!');

    }
}
