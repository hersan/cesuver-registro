<?php

namespace App\Http\Controllers;

use App\Entity\Place;
use App\Services\Schedule;
use Illuminate\Database\Eloquent\Builder;
use Mail;
use DB;
use Carbon\Carbon;
use App\Entity\User;
use App\Entity\Generator;
use App\Entity\Evaluation;
use Illuminate\Http\Request;
use App\Mail\AcceptanceDocumentNotification;

class AcceptanceDocumentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = User::findOrFail($request->id);
        $practical_dates = (new Schedule())->get('practico');
        $theoretical_dates = (new Schedule())->get('teorico');
        $places = Place::all()->pluck('name', 'id');
        return view(
            'acceptance_form',
            compact('user', 'practical_dates', 'theoretical_dates','places')
        );
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'scheduled_date' => 'required|string',
            'place' => 'required',
        ]);

        $schedule = new Schedule();

        try {
            DB::beginTransaction();

            /*$user = User::where('id',$request->id)->whereHas('evaluations', function ($query) {
                $query->where('status',Evaluation::OPEN);
            })->first();*/
            $user = User::query()
                ->whereId($request->id)
                ->with('evaluations')
                ->first();

            if (!$user->evaluations->isEmpty()) {
                return back()->with('warning', 'Este usuario ya tiene una evaluación programada')->withInput($request->toArray());
            }

            $place = Place::findOrFail($request->place);
            $generator = Generator::find(1);

            $previousEvaluation = Evaluation::whereUserId($request->id)->first();

            if (empty($previousEvaluation)) {
                $code = $generator->code(Carbon::now());
            } else {
                $code = $previousEvaluation->code;
            }

            $user = User::find($request->id);
            $evaluation = Evaluation::create([
                'code' => $code,
                'scheduled_date' => $schedule->getByKey($request->scheduled_date),
                'practical_date' => $schedule->getByKey($request->practical_date)
            ]);

            $place->evaluations()->save($evaluation);
            $user->evaluations()->save($evaluation);
            $user->status = User::PROGRAMMED;
            $user->save();

            $user->load('evaluations');
            Mail::to($user)->send(new AcceptanceDocumentNotification($user));

            DB::commit();

            return back()->with('status', 'Su información se agrego al sistema: ¡Gracias!');

        } catch (\Exception $e) {
            DB::rollBack();
            dump($e->getMessage());
            dump($e->getTraceAsString());
            return back()->with('status', 'Existe un error'. $e->getMessage());
        }
    }
}
