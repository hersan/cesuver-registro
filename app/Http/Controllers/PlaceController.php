<?php

namespace App\Http\Controllers;

use App\Entity\Place;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PlaceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = Place::all();
        return view('place.index', compact('places'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('place.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, ['name' => 'required|string|unique:places']);

        Place::firstOrCreate($request->except('_token'));

        return back()->with('status', 'La institución fue creada');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function show(Place $place)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function edit(Place $place)
    {
        return view('place.edit', compact('place'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Place $place)
    {
        $this->validate($request, ['name' => [
            'required',
            Rule::unique('places')->ignore($place->id),
        ]]);

        $place->update($request->all());

        return back()->with('status', 'La institución fue actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function destroy(Place $place)
    {
        try {
            $place->canDeleted();
        } catch (\Exception $exception) {
            return back()->with('warning', $exception->getMessage());
        }
        return back()->with('success', 'El elemento fue borrado');
    }
}
