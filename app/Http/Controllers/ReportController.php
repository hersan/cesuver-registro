<?php

namespace App\Http\Controllers;

use App\Mail\GlobalReport;
use Carbon\Carbon;
use DB;
use Excel;
use Mail;
use PDF;
use App\Entity\User;
use App\Entity\Evaluation;
use Illuminate\Http\Request;
use Prophecy\Argument\Token\ExactValueToken;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function generate(User $user)
    {
        $evaluations = Evaluation::with('scores')
            ->where('status','<>',2)
            ->whereUserId($user->id)
            ->first();
        $user->load('identities', 'profiles.subject');

        if($user->profiles->subject->scale != 2) {
            $scale = 'evaluation.scale';
            $max = config('evaluation.score');
        } else {
            $scale = 'evaluation.new_global_scale';
            $max = config('evaluation.max_new_practical_scale');
        }

        $total_score = $evaluations->scores->pluck('score')->sum();
        $total_calculate_score = $evaluations->scores->pluck('calculate_score')->sum() / 2;
        $competitions = collect(config($scale))->map(function ($scale) use ($total_score) {
            if ($total_score >= $scale['from'] && $total_score <= $scale['to']) {
                return $scale['value'];
            }
        })->filter()->values()->get(0);

        $pdf = PDF::loadView(
            'result.global_result_pdf',
            compact('evaluations', 'total_score', 'total_calculate_score', 'competitions', 'user','scale')
        );

        return $pdf->download('reporte global de resultados.pdf');

    }

    public function generateTheoretical(User $user)
    {
        $user->load('profiles.subject');

        if($user->profiles->subject->scale != 2) {
            $scale = 'evaluation.theoretical_scale';
            $max = config('evaluation.score');
        } else {
            $scale = 'evaluation.new_theoretical_scale';
            $max = config('evaluation.max_new_theoretical_scale');
        }

        $evaluation = Evaluation::with('theoreticalScore')
            ->where('status','<>',2)
            ->whereUserId($user->id)
            ->firstOrFail();

        $score = $evaluation->scores->first();

        $user->load('identities', 'profiles');

        $competitions = collect(config($scale))->map(function ($item) use ($score) {
            if ($score->score >= $item['from'] && $score->score <= $item['to']) {
                return [
                    'value' => $item['value'],
                    'to' => $item['to']
                ];
            }
        })->filter()->collapse()->all();

        $pdf = PDF::loadView(
            'result.theoretical_result_pdf',
            compact('evaluation','score', 'user', 'competitions','scale','max')
        );

        return $pdf->download("{$user->identities->full_name}-reporte_teorico.pdf");
    }

    public function generatePractical(User $user)
    {
        $user->load('identities', 'profiles.subject');

        if($user->profiles->subject->scale != 2) {
            $scale = 'evaluation.theoretical_scale';
            $max = config('evaluation.score');
        } else {
            $scale = 'evaluation.new_practical_scale';
            $max = config('evaluation.max_new_practical_scale');
        }

        $evaluation = Evaluation::with('practicalScore')
            ->where('status','<>',2)
            ->where('user_id',$user->id)
            ->firstOrFail();

        $score = $evaluation->scores->filter(function($score){
            return $score->evaluation_type == 'Evaluación Práctica';
        })->first();

        $competitions = collect(config($scale))->map(function ($item) use ($score) {
            if ($score->score >= $item['from'] && $score->score <= $item['to']) {
                return [
                    'value' => $item['value'],
                    'to' => $item['to']
                ];
            }
        })->filter()->collapse()->all();

        $pdf = PDF::loadView(
            'result.practical_result_pdf',
            compact('evaluation', 'score', 'user', 'competitions','scale','max')
        );

        return $pdf->download("{$user->identities->full_name}-reporte_practico.pdf");
    }

    public function showSearchForm(Request $request)
    {
        return view('user.search');
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required|date',
            'end_date'   => 'required_with:start_date',
        ]);

        $users = User::whereStatus(1)->whereIsAdmin(false)->with([
            'profiles.subject',
            'identities',
            'evaluation' => function ($query) use ($request) {
                if ($request->has('start_date') && $request->has('end_date')) {
                    $query->whereBetween('created_at', [
                        Carbon::createFromFormat('Y-m-d', $request->start_date),
                        Carbon::createFromFormat('Y-m-d', $request->end_date)
                    ]);
                }
                else {
                    $query->WhereDate('created_at', Carbon::createFromFormat('Y-m-d', $request->start_date));
                }
            }
        ])->get();
        $users->load('evaluation');

        if (empty($users)) {
            return back()
                ->with('warning', 'No existen registros para esta búsqueda')
                ->withInput($request->toArray())
            ;
        }

        return view('user.show_search', compact('users'));
    }

    public function showSearchConcentratedForm()
    {
        return view('user.search_concentrated');
    }

    public function searchConcentrated(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required|date',
            'end_date'   => 'required_with:start_date',
        ]);

        //$users->load('evaluation.place');
        $result = DB::select("
            select 
		ev.code folio,
		i.curp curp, 
		e.code folio, 
		i.second_name apellido_paterno, 
		i.last_name apellido_materno, 
		i.first_name nombre,
		s.name licenciatura,
		theoretical.score puntaje_teorico,
		theoretical.calculate_score calificacon_teorica,
		practical.score puntaje_practico,
		practical.calculate_score calificacon_practico,
		scores.puntaje puntaje_global,
		round(scores.calificacion/2,2) as calificacion_global
from users u 
    join identities i
		on i.user_id = u.id
	join evaluations e
		on e.user_id = u.id
	join profiles p
		on p.user_id = u.id
	join subjects s
		on s.id = p.subject_id
	join evaluations ev
		on ev.user_id = u.id
    join (
        select
			ev.evaluation_id id,
			sum(ev.score) as puntaje,
			sum(ev.calculate_score) as calificacion
        from evaluation_scores ev
        	group by ev.evaluation_id
    ) as scores on ev.id = scores.id
	join (
        select
         evs.evaluation_id,
         evs.score score,
         evs.calculate_score calculate_score,
			if(evs.evaluation_type = 1, 'teorico', 'practico') types		
        from evaluation_scores evs
        where evs.evaluation_type = 1
    ) as theoretical on ev.id = theoretical.evaluation_id
    join (
        select
         evs.evaluation_id,
         evs.score score,
         evs.calculate_score calculate_score,
			if(evs.evaluation_type = 1, 'teorico', 'practico') types		
        from evaluation_scores evs
        where evs.evaluation_type = 2
    ) as practical on ev.id = theoretical.evaluation_id
	join evaluation_scores sc
		on sc.evaluation_id = ev.id
where u.is_admin = 0 and u.status = 1 and ev.created_at between :start_date and :end_date
            ", [
            'start_date' => Carbon::createFromFormat('Y-m-d', $request->start_date),
            'end_date'   => Carbon::createFromFormat('Y-m-d', $request->end_date)
        ]);

        $competitions = collect(config('evaluation.scale'));

        $result = collect($result)->map(function ($item) {
            return $this->objectToArray($item);
        })
            ->unique()
            ->map(function ($item) use ($competitions) {
                return $competitions->map(function ($scale) use ($item) {
                    if ($item['puntaje_global'] >= $scale['from'] && $item['puntaje_global'] <= $scale['to']) {
                        $array = collect($item)->put('resultado', $scale['value']);
                        return $array;
                    }
                });
            })->flatten(1)->filter();;

        Excel::create('calificaciones.xlsx', function ($excel) use ($result) {
            $excel->sheet('matricula', function ($sheet) use ($result) {
                $sheet->fromArray($result->toArray());
            });
        })->download('xlsx');
    }

    public function showSearchEnrollForm(Request $request)
    {
        return view('user.search_enroll');
    }

    public function searchEnroll(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required|date',
            'end_date'   => 'required_with:start_date',
        ]);

        //$users->load('evaluation.place');
        $result = DB::select("
            select 
		i.curp curp, 
		e.code folio, 
		i.second_name apellido_paterno, 
		i.last_name apellido_materno, 
		i.first_name nombre,
		DATE_FORMAT(i.birthday, '%d-%m-%Y') fecha_nacimiento,
		s.name licenciatura,
		IF(i.sex = 1, 'Maculino', 'Femenino') sexo,
		TIMESTAMPDIFF(YEAR, i.birthday, CURDATE()) edad,
		pl.name sede,
		DATE_FORMAT(e.scheduled_date, '%d-%m-%Y') examen_aplicacion
from users u
    join identities i
		on i.user_id = u.id
	join evaluations e
		on e.user_id = u.id
	join profiles p
		on p.user_id = u.id
	join subjects s
		on s.id = p.subject_id
	join evaluations ev
		on ev.user_id = u.id
	join places pl
		on pl.id = ev.place_id 
where u.is_admin = 0 and u.status = 1 and ev.created_at between :start_date and :end_date
            ", [
            'start_date' => Carbon::createFromFormat('Y-m-d', $request->start_date),
            'end_date'   => Carbon::createFromFormat('Y-m-d', $request->end_date)
        ]);

        $result = collect($result)->map(function ($item) {
            return $this->objectToArray($item);
        });

        Excel::create('matricula.xlsx', function ($excel) use ($result) {
            $excel->sheet('matricula', function ($sheet) use ($result) {
                $sheet->setColumnFormat(array(
                    'F' => 'dd-mm-YY',
                    'K' => 'dd-mm-YY'
                ));
                $sheet->fromArray($result);
            });
        })->download('xlsx');
    }

    public function showSearchScoreForm(Request $request)
    {
        return view('user.search_score');
    }

    public function searchScore(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required|date',
            'end_date'   => 'required_with:start_date',
        ]);

        $users = User::whereStatus(1)->whereIsAdmin(false)->with([
            'identities',
            'profiles.subject',
            'identities',
            'evaluation.place',
            'evaluation' => function ($query) use ($request) {
                if ($request->has('start_date') && $request->has('end_date')) {
                    $query->whereBetween('created_at', [
                        Carbon::createFromFormat('Y-m-d', $request->start_date),
                        Carbon::createFromFormat('Y-m-d', $request->end_date)
                    ]);
                }
                else {
                    $query->WhereDate('created_at', Carbon::createFromFormat('Y-m-d', $request->start_date));
                }
            }
        ])->get();
        return $users;
    }

    public function objectToArray($data)
    {
        if (is_object($data)) {
            return get_object_vars($data);
        }
    }
}
