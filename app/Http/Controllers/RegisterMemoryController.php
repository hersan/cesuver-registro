<?php

namespace App\Http\Controllers;

use App\Entity\Evaluation;
use App\Entity\Profile;
use App\Entity\User;
use App\Mail\DescriptiveMemoryReception;
use Illuminate\Http\Request;
use Mail;

class RegisterMemoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('memories_form');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'descriptive_memory' => 'required|mimes:pdf',
        ]);

        $profiles = User::whereId(auth()->id())->has('profiles')->first();

        if($profiles == null) {
            return back()->with('warning', 'Necesita registrar una solicitud');
        }

        $evaluation = User::whereId(auth()->id())->has('evaluations')->first();

        if($evaluation == null) {
            return back()->with('warning', 'Necesita tener un evaluación programada');
        }

        $profile = Profile::whereUserId(auth()->id())->first();

        if ($profile->hasCaseStudy()) {
            return back()->with(
                'warning',
                'No se puede guardar si ya tienes una responsiva de caso práctico'
            );
        }

        $profile->addDocument('descriptive_memory', $request);

        $user = auth()->user();
        $user->load('evaluations','identities', 'profiles');

        Mail::to(auth()->user())->send(new DescriptiveMemoryReception($user));

        return back()->with('status', 'Su Memoria descriptiva se agrego al sistema: ¡Gracias!');

    }
}
