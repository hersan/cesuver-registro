<?php

namespace App\Http\Controllers;

use App\Entity\Evaluation;
use App\Entity\User;
use Illuminate\Http\Request;

class UserEvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Entity\User  $user
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $user->load('evaluations.place');

        return view('evaluation.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Entity\User  $user
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Evaluation $evaluation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Evaluation $evaluation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, Evaluation $evaluation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Evaluation $evaluation)
    {
        //
    }
}
