<?php

namespace App\Http\Controllers\Verification;

use Auth;
use App\Entity\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VerificationController extends Controller
{
    public function verify(Request $request)
    {
        /*$this->validate($request, [
            'code' => 'required|string|max:40',
        ]);*/

        $user = User::whereConfirmationCode($request->code)->firstOrFail();

        Auth::login($user);

        $user->confirmation_code = null;
        $user->confirmed = true;
        $user->save();

        return redirect('/home');
    }
}
