<?php

namespace App\Http\Controllers;

use App\Entity\Schedule;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::all();

        return view('schedule.index', compact('schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('schedule.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'date_at' => [
                'required',
                'date',
                'after_or_equal:today',
                'unique:schedules',
            ],
            'type' => [
                'required',
                'string',
            ]
        ]);

        Schedule::firstOrCreate($request->except('_token'));

        return back()->with('status', 'La fecha fue agregada');
    }

    /**
     * Display the specified resource.
     *
     * @param Schedule $schedule
     * @return void
     */
    public function show(Schedule $schedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        return view('schedule.edit', ['schedule' => $schedule]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
        $this->validate($request, [
            'date_at' => [
                'required',
                'date',
                'after_or_equal:today',
                Rule::unique('schedules')->ignore($schedule->id),
            ],
            'type' => [
                'required',
                'string',
            ]
        ]);

        $schedule->update($request->all());

        return back()->with('status', 'La fecha fue actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Schedule $schedule
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Schedule $schedule) : RedirectResponse
    {
        $schedule->delete();

        return back()->with('success', 'El elemento fue borrado');
    }
}
