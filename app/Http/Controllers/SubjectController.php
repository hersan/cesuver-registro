<?php

namespace App\Http\Controllers;

use App\Entity\Subject;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all();

        return view('subject.index', compact('subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subject.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|string|unique:places']);

        Subject::firstOrCreate($request->except('_token'));

        return back()->with('success', 'Licenciatura creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        return view('subject.edit', compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        $this->validate($request, [
            'name' => ['required', Rule::unique('places')->ignore($subject->id)],
            'scale' => ['required']
        ]);

        $subject->update($request->all());

        return back()->with('success', 'La licenciatura fue actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        try {
            $subject->canDeleted();
        } catch (\Exception $exception) {
            return back()->with('warning', $exception->getMessage());
        }
        return back()->with('success', 'El elemento fue borrado');
    }
}
