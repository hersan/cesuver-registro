<?php

namespace App\Http\Controllers;

use App\Entity\Address;
use App\Entity\Identity;
use App\Entity\Profile;
use App\Entity\Subject;
use App\Entity\Telephone;
use App\Entity\User;
use App\Mail\ConfirmationOfTheRequest;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Mail;

class HomeController extends Controller
{
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all()->pluck('name','id');

        return view('home', compact('subjects'));
    }

    public function register(Request $request)
    {

        $this->validate($request, $this->useRulesAccordingToTheRequest($request));

        $identities = User::whereId(auth()->id())->has('identities')->first();

        if($identities != null) {
            return back()->with('status', 'Su información ya fue agregada previamente');
        }

        $subject = Subject::find($request->degree_in);

        try {
            DB::beginTransaction();

            $identity = Identity::create(
                $request->only(config('entity.identity.fields'))
            );

            $address = Address::create(
                $request->only(config('entity.address.fields'))
            );

            collect($request->only([
                'telephones', 'telephone_type'
            ]))->transpose()->map(function($telephonesData){
                return new Telephone([
                    'number' => $telephonesData[0],
                    'telephone_type' => $telephonesData[1],
                ]);
            })->each(function ($telephone) {
                auth()->user()->telephones()->save($telephone);
            });

            $profile = Profile::create(
                $request->only(config('entity.profile.fields'))
            );

            $identity->addDocumentsFromRequest($request);

            $profile->addDocumentsFromRequest($request);

            auth()->user()->identities()->save($identity);
            auth()->user()->addresses()->save($address);
            auth()->user()->profiles()->save($profile);
           $subject->profiles()->save($profile);

            DB::commit();
            // do your database transaction here
            Mail::to(auth()->user())->send(new ConfirmationOfTheRequest(auth()->user()));
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return back()->with('status', 'Se presento un error.');
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('status', 'Se presento un error.');
        }

        return back()->with('status', 'Su información se agrego al sistema: ¡Gracias!');
    }

    public function useRulesAccordingToTheRequest(Request $request) {
        if ($request->option == 1) {
           return [
               'first_name' => 'required|string|min:3',
               'second_name' => 'required|string|min:3',
               //'last_name' => 'alpha',
               'sex' => 'required|numeric',
               'birthday' => 'required|date|date_format:Y-m-d|before:' . Carbon::now()->toDateString(),
               'curp' => 'required|alpha_num',
               'curp_file' => 'required|mimes:pdf',
               'identity_card' => 'required|mimes:pdf',
               'street' => 'required',
               'neighborhood' => 'required',
               'postal_code' => 'required|numeric|digits:5',
               'state' => 'required|string',
               'city' => 'required|string',
               'telephones.*' => 'required|numeric|digits:10',
               'telephone_type.*' => 'required|numeric',
               'degree_in' => 'required|string',
               'option' => 'required',
               'proof_of_sufficiency_file' => 'required_if:option,1|mimes:pdf',
               'terms' => 'required|boolean',
           ];
        } else {
            return [
                'first_name' => 'required|string|min:3',
                'second_name' => 'required|string|min:3',
                //'last_name' => 'alpha',
                'sex' => 'required|numeric',
                'birthday' => 'required|date|date_format:Y-m-d|before:' . Carbon::now()->toDateString(),
                'curp' => 'required|alpha_num',
                'curp_file' => 'required|mimes:pdf',
                'identity_card' => 'required|mimes:pdf',
                'street' => 'required',
                'neighborhood' => 'required',
                'postal_code' => 'required|numeric|digits:5',
                'state' => 'required|string',
                'city' => 'required|string',
                'telephones.*' => 'required|numeric|digits:10',
                'telephone_type.*' => 'required|numeric',
                'degree_in' => 'required|string',
                'option' => 'required',
                'curriculum' => 'required_if:option,0|required|mimes:pdf',
                'portfolio_of_evidence' => 'required_if:option,0|required|mimes:pdf',
                'terms' => 'required|boolean',
            ];
        }
    }
}
