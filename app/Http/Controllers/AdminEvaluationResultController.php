<?php

namespace App\Http\Controllers;

use App\Entity\Evaluation;
use App\Mail\PracticalResult;
use App\Mail\TheoreticalResult;
use App\Support\Calculator;
use DB;
use App\Entity\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Entity\EvaluationScore;
use App\Entity\AspectsEvaluated;
use Mail;

class AdminEvaluationResultController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, User $user)
    {
        return view('evaluation.create_results', compact('user'));
    }

    public function showPracticalForm(User $user, Evaluation $evaluation)
    {
        $user->load('identities');
        $evaluation->load('scores','practicalScore');
        return view('evaluation.practical_form', ['user' => $user, 'evaluation' => $evaluation]);
    }

    public function showTheoreticalForm(User $user, Evaluation $evaluation)
    {
        $user->load('identities');
        $evaluation->load('scores');
        return view('evaluation.theoretical_form', compact('user', 'evaluation'));
    }

    public function registerTheoretical(Request $request, User $user, Evaluation $evaluation)
    {
        $user->load('profiles.subject');

        if($user->profiles->subject->scale != 2) {
            $scale = 'evaluation.theoretical_scale';
            $max = 300;
        } else {
            $scale = 'evaluation.new_theoretical_scale';
            $max = config('evaluation.max_new_theoretical_scale');
        }

        $this->validate($request, [
            'evaluation_type' => 'required|in:1',
            'score' => "numeric|max:$max",

        ]);

        $scores = $evaluation->load('scores');

        if($scores->scores->isNotEmpty()) {
            return back()->with('danger', 'Ya se agregaron resultados para esta evaluación');
        }

        try {
            DB::beginTransaction();

            $score = new EvaluationScore([
                'evaluation_type' => $request->evaluation_type,
                'score' => $request->score,
                'calculate_score' => Calculator::score($request->score,$max),
            ]);

            $evaluation->status = Evaluation::APPLIED_THEORETICAL;
            $evaluation->save();
            $evaluation->scores()->save($score);


            $competitions = collect(config($scale))->map(function($item) use ($score) {
                if($score->score >= $item['from'] && $score->score <= $item['to']) {
                    return $item['value'];
                }
            })->filter()->values()->get(0);

            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            return back()->with('danger','Existe un error inesperado en la consulta ')->withInput($request->all());
        } catch ( \Exception $e) {
            DB::rollBack();
            return back()->with('danger','Existe un error inesperado')->withInput($request->all());
        }

        return back()->with('status', 'Se ingreso la puntuación de forma correcta');
    }

    public function registerPractical(Request $request, User $user, Evaluation $evaluation)
    {
        $user->load('profiles.subject');

        if($user->profiles->subject->scale != 2) {
            $scale = 'evaluation.theoretical_scale';
            $max = config('evaluation.score');
        } else {
            $scale = 'evaluation.new_practical_scale';
            $max = config('evaluation.max_new_practical_scale');
        }

        $this->validate($request, [
            'practical_evaluation_date' => 'nullable|date|after_or_equal:' . $evaluation->practical_date,
            'evaluation_type' => 'required|numeric|in:2',
            'aspects_evaluated.*' => 'required|string',
            'aspects_score.*' => 'required|numeric'
        ]);

        $totalScore = collect($request->only('aspects_score'))->flatten()->sum();

        if(!($totalScore <= $max)) {
            return back()->with('status', 'No se puede tener una calificación mayor a: ' . $max)
                ->withInput($request->all());
        }

        if($evaluation->status != 'Teórico Aplicado') {
            return back()->with('status', 'Agregue primero los resultados de la evaluación Teórica')
                ->withInput($request->all());
        }

        $evaluation->load('aspects');

        if($evaluation->aspects->isNotEmpty()) {
            return back()->with('status', 'Ya se agregaron resultados para esta evaluación')
                            ->withInput($request->all());
        }

        try {
            DB::beginTransaction();

            $aspects = collect($request->only([
                'aspects_evaluated', 'aspects_score'
            ]))->transpose()->map(function($aspectData){
                return new AspectsEvaluated([
                    'aspect' => $aspectData[0],
                    'score' => $aspectData[1],
                ]);
            })->each(function ($aspect) use($evaluation) {
                $evaluation->aspects()->save($aspect);
            });

            $evaluation->status = Evaluation::EVALUATION_COMPLETED;
            $evaluation->practical_evaluation_date = $request->get('practical_evaluation_date');
            $evaluation->save();

            $evaluation->update([
                'comments' => $request->comments,
            ]);

            $score = $evaluation->scores()->create([
                'evaluation_type' => $request->evaluation_type,
                'score' => $aspects->pluck('score')->sum(),
                'calculate_score' => Calculator::score($aspects->pluck('score')->sum(),$max),
            ]);

            $competitions = collect(config($scale))->map(function($item) use ($score) {
                if($score->score >= $item['from'] && $score->score <= $item['to']) {
                    return $item['value'];
                }
            })->filter()->values()->get(0);


            DB::commit();

            return back()->with('status', 'Su información se agrego al sistema: !Gracias!');

        } catch (QueryException $e) {
            DB::rollBack();
            return back()->with('danger', 'Se presento un error al intentar insertar los registros')->withInput($request->all());
        } catch (\Exception $e) {
            DB::rollBack();
        }   return back()->with('danger', 'Se presento un error inesperado.')->withInput($request->all());

    }

    public function register(Request $request, User $user)
    {
        $this->validate($request, [
            'evaluation_type.*' => 'required|numeric',
            'score.*' => 'required|numeric',
            'aspects_evaluated.*' => 'required|string',
            'aspects_score.*' => 'required|max:100'
        ]);

        $scores = User::whereId($user->id)->has('evaluation.scores')->first();

        if($scores != null) {
            return back()->with('status', 'Ya se agregaron resultados para esta evaluación');
        }

        try {
            DB::beginTransaction();

            collect($request->only([
                'evaluation_type', 'score'
            ]))->transpose()->map(function($scoreData){
                return new EvaluationScore([
                    'evaluation_type' => $scoreData[0],
                    'score' => $scoreData[1],
                    'calculate_score' => $scoreData[1],
                ]);
            })->each(function ($score) use($user) {
                $user->evaluation->scores()->save($score);
            });

            collect($request->only([
                'aspects_evaluated', 'aspects_score'
            ]))->transpose()->map(function($aspectData){
                return new AspectsEvaluated([
                    'aspect' => $aspectData[0],
                    'score' => $aspectData[1],
                ]);
            })->each(function ($aspect) use($user) {
                $user->evaluation->aspects()->save($aspect);
            });

            $user->evaluation()->update(['comments' => $request->comments]);

            DB::commit();
            // do your database transaction here
        } catch (QueryException $e) {
            DB::rollBack();
            return back()->with('status', 'Se presento un error.');
        }

        return back()->with('status', 'Su información se agrego al sistema: !Gracias!');
    }
}
