<?php

namespace App\Mail;

use App\Entity\User;
use PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CaseStudyReception extends Mailable
{
    use Queueable, SerializesModels;

    /** @var User */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pdf_file = PDF::loadView(
            'pdf.case_study',
            ['user' => $this->user]
        );

        return $this->subject('Acuse de recepción de reponsiva de Caso práctico')
            ->view('mail.case_study')
            ->attachData($pdf_file->output(), 'acuse_responsiva_caso_practico.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}
