<?php

namespace App\Mail;

use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDF;

class DescriptiveMemoryReception extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var User $user
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pdf_file = PDF::loadView(
            'pdf.descriptive_memory',
            ['user' => $this->user]
        );

        return $this->subject('Acuse de recepción de memoria descriptiva')
            ->view('mail.descriptive_memory')
            ->attachData($pdf_file->output(), 'Acuse de recepcion de memoria descriptiva.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}
