<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDF;

class GlobalReport extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var array
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pdf_file = PDF::loadView(
                'result.global_result_pdf',
                $this->data
        );

        return $this->subject('Reporte global de resultados')
            ->view('mail.global_result_pdf', $this->data)
            ->attachData($pdf_file->output(), 'Reporte global de resultados.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}
