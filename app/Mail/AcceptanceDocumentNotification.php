<?php

namespace App\Mail;

use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDF;

class AcceptanceDocumentNotification extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var User
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pdf_file = PDF::loadView(
            'pdf.acceptance_document',
            ['user' => $this->user]
        );

        return $this->subject('Carta de aceptación')
            ->view('mail.acceptance_document')
            ->attachData($pdf_file->output(), 'carta de aceptacion.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}
