<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDF;

class PracticalResult extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var array
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pdf_file = PDF::loadView(
            'result.practical_result_pdf',
            $this->data
        );

        return $this->subject('REPORTE INDIVIDUAL DE RESULTADOS (ORAL)')
            ->view('mail.practical_result_pdf', $this->data)
            ->attachData($pdf_file->output(), 'Reporte evaluación practica.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}
