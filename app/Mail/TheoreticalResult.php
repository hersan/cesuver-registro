<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use PDF;
use Illuminate\Contracts\Queue\ShouldQueue;

class TheoreticalResult extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var array
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pdf_file = PDF::loadView(
                'result.theoretical_result_pdf',
                $this->data
            );

        return $this->subject('EXAMEN TEÓRICO DE CONOCIMIENTOS DISCIPLINARIOS')
            ->view('mail.theoretical_result_pdf', $this->data)
            ->attachData($pdf_file->output(), 'Reporte evaluacion teorica.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}
