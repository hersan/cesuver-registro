<?php

namespace App\Mail;

use PDF;
use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmationOfTheRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $pdf_file = PDF::loadView(
            'pdf.confirmed_request',
            ['user' => $this->user]
        );

        return $this->subject('Acuse de solicitud')
                    ->view('pdf.confirmed_request')
                    ->attachData($pdf_file->output(), 'acuse de solicitud.pdf', [
                        'mime' => 'application/pdf',
                    ]);
    }
}
