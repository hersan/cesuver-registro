<?php

namespace App\Services;

class FloatToStringFormatter
{
    /**
     * Corta un número de tipo float a una posición después del punto decimal y lo regresa como string.
     *
     * @param float $number
     * @return string
     */
    public static function formatFloat($number)
    {
        // Convertir el número a string y usar la función explode para separar la parte entera y decimal.
        $numberParts = explode('.', (string) $number);

        // Si no existe una parte decimal, agregar ".0".
        if (!isset($numberParts[1])) {
            return $numberParts[0] . '.0';
        }

        // Tomar solo el primer dígito de la parte decimal.
        $decimalPart = substr($numberParts[1], 0, 1);

        // Retornar la cadena compuesta de la parte entera y el decimal.
        return $numberParts[0] . '.' . $decimalPart;
    }

}