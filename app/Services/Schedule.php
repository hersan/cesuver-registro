<?php
/**
 * Current File: Schedule.php
 * Project: cesuver-registro
 */

namespace App\Services;


use App\Contracts\ScheduleInterface;
use App\Entity\Schedule as ScheduleModel;
use Carbon\Carbon;

class Schedule implements ScheduleInterface
{

    public function get($type = null)
    {
        if (is_null($type)) {
            return ScheduleModel::whereYear('date_at', Carbon::now()->year)->get()->mapWithKeys(function ($schedule){
                return [$schedule->id => $this->format(Carbon::createFromFormat('Y-m-d', $schedule->date_at))];
            });
        } else {
            return ScheduleModel::whereYear('date_at', Carbon::now()->year)->where('type', $type)->get()->mapWithKeys(function ($schedule){
                return [$schedule->id => $this->format(Carbon::createFromFormat('Y-m-d', $schedule->date_at))];
            });
        }
    }

    public function getByKey($key)
    {
        return ScheduleModel::findOrFail($key)->date_at;
    }

    public function format($date)
    {
        $IntlDateFormatter = new \IntlDateFormatter(
            'es_ES',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE
        );

        return $IntlDateFormatter->format($date);
    }
}