<?php
/**
 * Created by PhpStorm.
 * User: Herminio
 * Date: 10/09/2017
 * Time: 23:28
 */

namespace App\Support;


use Carbon\Carbon;

class FormatDate
{
    public static function toLocale()
    {
        $date = new \DateTime();

        $IntlDateFormatter = new \IntlDateFormatter(
            'es_ES',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE
        );

        return $IntlDateFormatter->format($date);
    }

    public static function from($carbon)
    {
        if (is_null($carbon)) {
            return '';
        }

        $IntlDateFormatter = new \IntlDateFormatter(
            'es_ES',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE
        );

        return $IntlDateFormatter->format($carbon);
    }

    public static function practical(Carbon $carbon)
    {
        $carbon->addDays(7);

        $IntlDateFormatter = new \IntlDateFormatter(
            'es_ES',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE
        );

        return $IntlDateFormatter->format($carbon);
    }
}