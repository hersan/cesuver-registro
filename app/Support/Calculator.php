<?php
/**
 * Created by PhpStorm.
 * User: Herminio
 * Date: 01/09/2017
 * Time: 20:14
 */

namespace App\Support;


class Calculator
{
    public static function score($score, $maxScore)
    {
        $mark = ($score/(int) $maxScore) * 10;

        return sprintf('%0.2f', $mark);
    }
}