<?php
/**
 * Created by PhpStorm.
 * User: Herminio
 * Date: 01/09/2017
 * Time: 21:43
 */

namespace App\Support;


class EvaluationType
{
    private static $items = [
        1 => 'Evaluación Teórica',
        2 => 'Evaluación Práctica',
    ];

    public static function get()
    {
        return self::$items;
    }

    public static function getByKey($key)
    {
        return self::$items[$key];
    }
}