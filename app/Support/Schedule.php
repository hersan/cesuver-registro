<?php
/**
 * Created by PhpStorm.
 * User: Herminio
 * Date: 13/02/2018
 * Time: 19:04
 */

namespace App\Support;

use Carbon\Carbon;
use App\Contracts\ScheduleInterface;

class Schedule implements ScheduleInterface
{
    private $dates = [
        1 => '2018-03-24',
        2 => '2018-06-23',
        3 => '2018-09-22',
        4 => '2018-12-15',
        5 => '2019-03-23',
        6 => '2019-03-24',
        7 => '2019-04-20',
        8 => '2019-04-21',
        9 => '2019-05-18',
        10 => '2019-05-19',
        11 => '2019-06-22',
        12 => '2019-06-23',
        13 => '2019-07-20',
        14 => '2019-07-21',
        15 => '2019-08-17',
        16 => '2019-08-18',
        17 => '2019-09-21',
        18 => '2019-09-22',
        19 => '2019-10-19',
        20 => '2019-10-20',
        21 => '2019-11-16',
        22 => '2019-11-17',
        23 => '2019-12-07',
        24 => '2019-12-08',
    ];

    public function get()
    {
        return collect($this->dates)
            ->map(function($item) {
                return Carbon::instance(new \DateTime($item));
            })
            ->filter(function ($item){
                return $item->year === Carbon::now()->year;
            })
            ->transform(function($item){
                return $this->format($item);
        });
    }

    public function getByKey($key)
    {
        return $this->dates[$key];
    }

    public function format($date)
    {
        $IntlDateFormatter = new \IntlDateFormatter(
            'es_ES',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE
        );

        return $IntlDateFormatter->format($date);
    }
}