<?php
/**
 * Created by PhpStorm.
 * User: Herminio
 * Date: 19/08/2017
 * Time: 15:39
 */

trait GetCurrentUser
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();

            return $next($request);
        });

        //\View::share('user', $this->user);
    }
}