<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Carbon\Carbon;
use App\Entity\User;
use Facades\App\Support\Schedule;
use App\Mail\ConfirmationOfTheRequest;
use App\Mail\DescriptiveMemoryReception;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/view/test', function () {
    $user = User::with('identities','evaluation', 'profiles')->find(2);
    //dd($user);
    Mail::to(auth()->user())->send(new DescriptiveMemoryReception($user));
    return view('mail.descriptive_memory', compact('user'));
});

Route::get('test/facade', function() {
    $dates = Schedule::get();
    dd($dates);
});

Route::get('/pdf/footer', function(){
    $date = new \DateTime();

    $IntlDateFormatter = new IntlDateFormatter(
        'es_ES',
        IntlDateFormatter::LONG,
        IntlDateFormatter::NONE
    );
    $today = $IntlDateFormatter->format($date);

    return view('pdf.footer', compact('today'));
})->name('pdf.footer');

Route::get('/pdf/global/footer', function(){
    return view('pdf.global_footer');
})->name('global.pdf.footer');

Auth::routes();

Route::middleware(['auth'])->group(function (){
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/verification/{code}', 'Verification\VerificationController@verify')->name('verification');

    Route::post('/register/perfil', 'HomeController@register')->name('perfil');

    Route::get('/profile/{user}', 'AdminUserController@index')->name('get.user.profile');
    Route::post('/create/profile', 'AdminUserController@register')->name('post.user.profile');

    Route::get('/register/memory', 'RegisterMemoryController@index')->name('register.memory');
    Route::post('/register/memory', 'RegisterMemoryController@register');

    Route::get('/register/case', 'RegisterCaseStudyController@index')->name('register.case');
    Route::post('/register/case', 'RegisterCaseStudyController@register');

    Route::get('/applicant/results/{user}', 'ResultsController@index')->name('applicant.results');
    Route::get('/admin/results/{user}', 'ResultsController@index')->name('admin.results');

    Route::get('/acceptance/{id}', 'AcceptanceDocumentController@index')->name('acceptance');
    Route::post('/acceptance/{id}', 'AcceptanceDocumentController@register');

    Route::get('/create/results/{user}', 'AdminEvaluationResultController@index')->name('create.results');
    Route::post('/create/results/{user}', 'AdminEvaluationResultController@register');

    Route::get('/create/practical/{user}/{evaluation}', 'AdminEvaluationResultController@showPracticalForm')->name('create.practical');
    Route::post('/create/practical/{user}/{evaluation}', 'AdminEvaluationResultController@registerPractical');

    Route::get('/create/theoretical/{user}/{evaluation}', 'AdminEvaluationResultController@showTheoreticalForm')->name('create.theoretical');
    Route::post('/create/theoretical/{user}/{evaluation}', 'AdminEvaluationResultController@registerTheoretical');

    Route::get('global/report/{user}','ReportController@generate')->name('global.report');
    Route::get('theoretical/report/{user}','ReportController@generateTheoretical')->name('theoretical.report');
    Route::get('practical/report/{user}','ReportController@generatePractical')->name('practical.report');

    Route::get('user/download', 'Usercontroller@download');

    Route::resource('users','UserController');

    Route::get('/user/search', 'ReportController@showSearchForm')->name('user.search');
    Route::post('/user/search', 'ReportController@search');

    Route::get('/user/enroll', 'ReportController@showSearchEnrollForm')->name('user.enroll');
    Route::post('/user/enroll', 'ReportController@searchEnroll');

    Route::get('/user/concentrated', 'ReportController@showSearchConcentratedForm')->name('user.concentrated');
    Route::post('/user/concentrated', 'ReportController@searchConcentrated');

    Route::get('/user/scores', 'ReportController@showSearchScoreForm')->name('user.score');
    Route::post('/user/scores', 'ReportController@searchScore');

    Route::post('status/evaluation', 'StatusController@change');

    Route::resource('places', 'PlaceController');
    Route::resource('subjects', 'SubjectController');
    Route::resource('evaluations', 'EvaluationController');
    Route::resource('users.evaluations', 'UserEvaluationController');
    Route::resource('schedules', 'ScheduleController');
});