@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if (session('status'))
            <div class="alert alert-success">
                <p>{{ session('status') }}</p>
            </div>
            @endif

            @if (session('warning'))
            <div class="alert alert-warning">
                <p>{{ session('warning') }}</p>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Agregar reponsiva caso práctico</div>

                <div class="panel-body">
                    {!! Form::open(['route' => 'register.case', 'method' => 'post', 'files' => 'true', 'class' => 'form-horizontal']) !!}

                    <div class="form-group{{ $errors->has('case_study') ? ' has-error' : '' }}">
                        <label for="case_study" class="col-md-4 control-label"></label>

                        <div class="col-md-6">
                            <input id="case_study" type="file" class="form-control" name="case_study" value="{{ old('case_study') }}" autofocus>

                            @if ($errors->has('case_study'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('case_study') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Guardar responsiva
                            </button>

                            <br>

                        </div>

                    </div>
<!--                    <p align="center"><a href="{{url('user/download')}}">Manual para presentar la Memoria Descriptiva de Experiencia Laboral</a></p>-->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection