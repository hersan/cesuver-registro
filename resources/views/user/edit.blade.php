@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if (session('status'))
                    <div class="alert alert-success">
                        <p>{{ session('status') }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Perfil</div>

                    <div class="panel-body" id="app">
                        {!! Form::open(['route' => ['users.update', $user->id], 'method' => 'put', 'files' => 'true']) !!}

                        <fieldset>
                            <legend>Identidad</legend>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label for="first_name">Nombre</label>
                                        <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name', $user->identities->first_name) }}"  autofocus>

                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('second_name') ? ' has-error' : '' }}">
                                        <label for="last_name">Apellido Paterno</label>
                                        <input id="last_name" type="text" class="form-control" name="second_name" value="{{ old('second_name', $user->identities->second_name) }}"  autofocus>

                                        @if ($errors->has('second_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('second_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label for="last_name">Apellido Materno</label>
                                        <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name', $user->identities->last_name) }}"  autofocus>

                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div> <!-- END ROW -->

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('curp') ? ' has-error' : '' }}">
                                        <label for="curp">CURP</label>
                                        <input id="curp" type="text" class="form-control" name="curp" value="{{ old('curp', $user->identities->curp) }}"  autofocus>

                                        @if ($errors->has('curp'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('curp') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                                        <label for="sex" >Sexo</label>
                                        {!! Form::select('sex', [1 => 'Masculino', 2 => 'Femenino'], old('sex', $user->identities->sex), ['class' => 'form-control']) !!}

                                        @if ($errors->has('sex'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('sex') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                        <label for="birthday" >Fecha de nacimiento</label>
                                        <input id="birthday" type="date" class="form-control" name="birthday" value="{{ old('birthday', $user->identities->birthday->format('Y-m-d')) }}"  autofocus>

                                        @if ($errors->has('birthday'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('birthday') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group{{ $errors->has('curp_file') ? ' has-error' : '' }}">
                                        <label for="curp_file">CURP Pdf</label>
                                        <input id="curp_file" type="file" class="form-control" name="curp_file" value="{{ old('curp_file') }}"  autofocus>

                                        @if ($errors->has('curp_file'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('curp_file') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group{{ $errors->has('identity_card') ? ' has-error' : '' }}">
                                        <label for="identity_card">Identificación</label>
                                        <input id="identity_card" type="file" class="form-control" name="identity_card" value="{{ old('identity_card') }}"  autofocus>

                                        @if ($errors->has('identity_card'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('identity_card') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </fieldset> <!-- end identity -->

                        <fieldset>
                            <legend>Domicilio Particular</legend>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                        <label for="street">Calle y numero</label>
                                        <input id="street" type="text" class="form-control" name="street" value="{{ old('street', $user->addresses->street) }}" >

                                        @if ($errors->has('street'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('street') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('neighborhood') ? ' has-error' : '' }}">
                                        <label for="neighborhood">Colonia</label>
                                        <input id="neighborhood" type="text" class="form-control" name="neighborhood" value="{{ old('neighborhood',$user->addresses->neighborhood) }}" >

                                        @if ($errors->has('neighborhood'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('neighborhood') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label for="city">Municipio</label>
                                        <input id="city" type="text" class="form-control" name="city" value="{{ old('city', $user->addresses->city) }}" >

                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div> <!-- end row -->

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                        <label for="postal_code">C.P.</label>
                                        <input id="postal_code" type="text" class="form-control" name="postal_code" value="{{ old('postal_code', $user->addresses->postal_code) }}" >

                                        @if ($errors->has('postal_code'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('postal_code') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                        <label for="state">Estado</label>
                                        <input id="state" type="text" class="form-control" name="state" value="{{ old('state', $user->addresses->state) }}" >

                                        @if ($errors->has('state'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- end row -->

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group{{ $errors->has('references') ? ' has-error' : '' }}">
                                        <label for="references">Referencia</label>
                                        <input id="references" type="text" class="form-control" name="references" value="{{ old('references', $user->addresses->references) }}" >

                                        @if ($errors->has('references'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('references') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div> <!-- end row -->
                        </fieldset><!-- end address -->

                        <fieldset>
                            <legend>Datos de contacto</legend>
                            @foreach($user->telephones as $telephone)
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group{{ $errors->has('telephones.'.$loop->iteration) ? ' has-error' : '' }}">
                                            <label for="telephones">Teléfono</label>
                                            <input id="telephones" type="text" class="form-control" name="telephones[{{$loop->iteration}}]" value="{{ old('telephones.'.$loop->iteration, $telephone->number) }}" >

                                            @if ($errors->has('telephones.'.$loop->iteration))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('telephones.'.$loop->iteration) }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group{{ $errors->has('telephone_type.'.$loop->iteration) ? ' has-error' : '' }}">
                                            <label for="telephone_type">Tipo</label>
                                            {!! Form::select("telephone_type[$loop->iteration]", [1 => 'Principal', 2 => 'Secundario'], old('telephone_type.'.$loop->iteration, $telephone->telephone_type), ['class' => 'form-control']); !!}

                                            @if ($errors->has('telephone_type.'.$loop->iteration))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('telephone_type.'.$loop->iteration) }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </fieldset> <!-- end contact data -->

                        <fieldset>
                            <legend>Perfil</legend>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group{{ $errors->has('degree_in') ? ' has-error' : '' }}">
                                        {{--<label for="degree_in">Licenciatura</label>--}}
                                        {{--<input id="degree_in" type="text" class="form-control" name="degree_in" value="{{ old('degree_in') }}" >--}}
                                        {!! Form::select('degree_in', $subjects, old('degree_in', $user->profiles->subject->id), ['class' => 'form-control']) !!}
                                        @if ($errors->has('degree_in'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('degree_in') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="option" value="1" v-model="picked"> Constancia de suficiencia
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="option" value="0" v-model="picked"> Curriculum y Portafolio
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end row -->

                            <div class="row" v-show="picked == 1">
                                <div class="col-md-5">
                                    <div class="form-group{{ $errors->has('proof_of_sufficiency_file') ? ' has-error' : '' }}">
                                        <label for="proof_of_sufficiency_file">Constancia de suficiencia</label>
                                        <input id="proof_of_sufficiency_file" type="file" class="form-control" name="proof_of_sufficiency_file" value="{{ old('proof_of_sufficiency_file') }}" >

                                        @if ($errors->has('proof_of_sufficiency_file'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('proof_of_sufficiency_file') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row" v-show="picked == 0">
                                <div class="col-md-5">
                                    <div class="form-group{{ $errors->has('curriculum') ? ' has-error' : '' }}">
                                        <label for="curriculum">Curriculum</label>
                                        <input id="curriculum" type="file" class="form-control" name="curriculum" value="{{ old('curriculum') }}" >

                                        @if ($errors->has('curriculum'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('curriculum') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group{{ $errors->has('portfolio_of_evidence') ? ' has-error' : '' }}">
                                        <label for="portfolio_of_evidence">Portafolio de evidencias</label>
                                        <input id="portfolio_of_evidence" type="file" class="form-control" name="portfolio_of_evidence" value="{{ old('portfolio_of_evidence') }}" >

                                        @if ($errors->has('portfolio_of_evidence'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('portfolio_of_evidence') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </fieldset><!-- end Profile -->

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-1">
                                <button type="submit" class="btn btn-primary">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Terminos y condiciones</h4>
                </div>
                <div class="modal-body">
                    <h4>Reglamento aplicable para los usuarios interesados en presentar el Examen General de Acreditación de Saberes Adquiridos.</h4>
                    <p>El presente documento tiene como fin establecer las condiciones en que se llevará a cabo el Proceso General de Evaluación, los términos y condiciones, las reglas de aplicación, los derechos del usuario así como las restricciones para la presentación de la aplicación de acuerdo con las siguientes</p>
                    <h4>CLAUSULAS:</h4>
                    <P>PRIMERA.- El CENTRO DE ESTUDIOS SUPERIORES DE VERACRUZ en su calidad de Institución Evaluadora será la responsable de administrar los instrumentos de evaluación garantizando en todo momento la seguridad del contenido de los mismos, por ello el usuario debe sujetarse a las indicaciones que le sean anticipadas para la aplicación de los Exámenes.</P>
                    <h4>RESPONSABILIDADES Y DERECHOS DEL USUARIO:</h4>
                    <p>SEGUNDA.- El usuario tendrá derecho a la presentación del Examen General de Acreditación de Saberes Adquiridos consistente en una etapa denominada Examen Teórico de Conocimientos Disciplinarios y una segunda etapa denominada Examen de Conocimientos Aplicados, siempre que se hayan cubierto los aranceles para la aplicación de las evaluaciones y se haya obtenido de la Institución Evaluadora la Carta Aceptación – Pago de Derechos.</p>
                    <p>TERCERA.- El usuario podrá iniciar el proceso de inscripción a través de una SEDE autorizada por parte de la Institución Evaluadora, siempre que las mismas puedan otorgar beneficios para el pago de los aranceles como el uso de tarjetas bancarias.</p>
                    <p>CUARTA.- El usuario deberá presentarse de manera puntual a presentar los Exámenes que le sean programados en el lugar y fechas que le sean asignados.</p>
                    <p>QUINTA.- Para la presentación del Examen Teórico de Conocimientos Disciplinarios, el usuario deberá llegar con anticipación al inicio de la evaluación debiendo acreditarse ante el aplicador con una identificación oficial vigente en original, pudiendo ser la credencial de elector, el pasaporte, o alguna otra identificación expedida por el gobierno federal, estatal o municipal siempre que cuente con la fotografía, firma y CURP del interesado.</p>
                    <p>SEXTA.- El usuario deberá llevar para el día del Examen Teórico de Conocimientos Disciplinarios 2 lápices del punto número 2 con suficiente punta, un lapicero de tinta negra o azul, una goma blanca, 2 hojas blancas tamaño carta, una calculadora que para el caso de las Licenciaturas en cualquier Ingeniería deberá ser de tipo científica.</p>
                    <p>SÉPTIMA.- No se permitirá el uso de teléfonos celulares durante el desarrollo de la evaluación, aun para usarse como calculadora.</p>
                    <p>OCTAVA.- La aplicación escrita se llevará a cabo a través de un cuadernillo, el cual no podrá rayarse ni maltratarse. Para emitir las respuestas se otorgará una hoja de respuestas en donde deberá rellenar de forma completa la opción que mejor considere acorde a la respuesta.</p>
                    <p>NOVENA.- La aplicación escrita está considerada para desarrollarse en un lapso de 5 horas, por lo que es necesario ir preparado para dicha circunstancia.</p>
                    <p>DÉCIMA.- El aplicador acordará con el grupo una hora intermedia para realizar un pequeño receso y posterior a ello continuar con la aplicación.</p>
                    <p>DÉCIMA PRIMERA.- Los usuarios que no se presenten a la aplicación a la hora indicada no podrán ser evaluados en la fecha programada, sin embargo no perderán el derecho a su evaluación, debiendo coordinarse con la SEDE en la que inició el proceso de inscripción para ser reprogramado conforme al calendario oficial establecido.
                        Esta clásula solo será aplicable para aquellos usuarios que por alguna razón de causa de fuerza mayor o caso forituito llegasen a faltar a la presentación del examen en cualquiera de sus etapas, siempre que, por escrito, informen a la Insitución Evaluadora o a la SEDE, las razones justificadas de dicha circunstancia debiendo aportar los elementos suficientes que demuestren su dicho. De no darse el aviso bajo las condiciones señaladas, el pago del arancel se perderá y no se tendrá derecho a su devolución.</p>
                    <p>DÉCIMA SEGUNDA.- Los usuarios deberán respetar en todo momento las indicaciones del aplicador y seguir las instrucciones del mismo para no afectar el inicio y desarrollo de la aplicación.</p>
                    <p>DÉCIMA TERCERA.- Todos los usuarios, con independencia del resultado obtenido en el Examen Teórico de Conocimientos Disciplinarios, deberán presentar conforme al calendario autorizado, su trabajo de “Memoria Descriptiva de Experiencia Profesional” conforme al manual diseñado para su presentación.</p>
                    <p>DÉCIMA CUARTA.- Los usuarios que no presenten el trabajo de “Memoria Descriptiva de Experiencia Profesional” no podrán ser evaluados en la segunda fase denominada “Examen de Conocimientos Aplicados” debido a que el trabajo de referencia será la fuente de información principal que el Sínodo tomará en cuenta para realizar la valoración de las competencias del interesado.</p>
                    <p>DÉCIMA QUINTA.- La segunda etapa del Examen General de Acreditación de Saberes Adquiridos, consta de un trabajo denominado “Memoria Descriptiva de Experiencia Profesional”, y de una sustentación oral, misma que se realizará ante un Sínodo imparcial, compuesto por un Presidente (Evaluador), un secretario y un vocal.</p>
                    <p>DÉCIMA SEXTA.- La evaluación oral se realizará a puerta cerrada por lo que no podrá acceder a las instalaciones persona ajena al Sínodo, el personal asignado como ayuda por parte de la SEDE y el sustentante.</p>
                    <p>DÉCIMA SÉPTIMA.- El usuario es consciente y autoriza que el desarrollo de la evaluación oral se video grabe, con el fin de darle certeza jurídica al usuario y a las partes que intervienen en el desarrollo del Examen.</p>
                    <p>DÉCIMA OCTAVA.- El usuario tendrá derecho a recibir de la Institución Evaluadora el “Reporte Individual de Resultados” solo para el caso del resultado del Examen Teórico; el “Reporte Global de Resultados” y el “Dictamen de Acreditación” cuando haya concluido ambos procesos con la misma Institución Evaluadora.</p>
                    <p>DÉCIMA NOVENA.- En caso de que el usuario no esté de acuerdo con sus resultados, podrá hacer el uso del derecho de la revisión de resultados, debiendo iniciar el proceso llenando el formato “Solicitud de Revisión de Resultados” y presentarlo ante la SEDE en la que inició el proceso de inscripción o bien ante la Institución Evaluadora.</p>
                    <p>VIGÉSIMA.- El usuario podrá solicitar a la Institución Evaluadora que le realice el trámite de titulación ante la DGAIR, debiendo autorizar a través del formato correspondiente a las personas que la Institución le indique. Para ello deberá cubrir el total del pago por el servicio prestado.</p>
                    <p>VIGÉSIMA PRIMERA.- No se reembolsarán los importes de pago por concepto de aranceles para la aplicación de las evaluaciones, cuando el usuario se haya presentado a realizar al menos una de las dos etapas de evaluación, aun cuando el resultado le sea o no favorable.</p>
                    <p>VIGÉSIMA SEGUNDA.- El usuario tendrá derecho al reembolso del pago de sus aranceles, siempre que se desista por escrito del proceso general de evaluación y dicha situación la haga de conocimiento a la Institución Evaluadora al menos con 48 horas de anticipación.</p>
                    <p>VIGÉSIMA TERCERA.- No se permitirá el acceso a las instalaciones de las SEDES o de la Institución Evaluadora a personas que se presenten en evidente estado de ebriedad o bajo la influencia de alguna droga. Tampoco serán aceptadas personas que bajo amenazas o intimidación alguna exijan un trato especial, alteren el orden público o inciten a otros a su alteración.</p>
                    <p>VIGÉSIMA CUARTA.- El usuario que abandone la aplicación del examen general de evaluación en cualquier momento del desarrollo de la misma, perderá el derecho del pago de los aranceles cubiertos, así como el derecho para continuar con el proceso general de evaluación.</p>
                    <p>VIGÉSIMA QUINTA.- Los resultados de las evaluaciones se entregarán a los usuarios vía electrónica en un término de 10 días posteriores a la fecha de la evaluación.</p>
                    <p>VIGÉSIMO SEXTA.- En todo momento el usuario, en desacuerdo con alguna de las etapas del proceso general de evaluación, podrá a través del formato “Solicitud de Reclamación”, presentar su inconformidad ante la Institución Evaluadora.</p>
                    <h4>DE LAS OBLIGACIONES DE LA INSTITUCIÓN EVALUADORA:</h4>
                    <p>PRIMERA.- La Institución Evaluadora deberá en todo momento garantizar el acceso al proceso general de evaluación a cualquier persona que lo solicite.</p>
                    <p>SEGUNDA.- La Institución Evaluadora, debe en todo momento respetar al usuario, ofreciéndole las condiciones adecuadas para ser atendido con amabilidad y respeto.</p>
                    <p>TERCERA.- La Institución Evaluadora respetará las fechas autorizadas para la aplicación de los Exámenes por lo que no puede variarlas por ningún motivo.</p>
                    <p>CUARTA.- La Institución Evaluadora, entregará en el plazo de 10 días posteriores a la fecha de presentación de los Exámenes, los resultados a los usuarios, los cuales deberá publicar en su portal de internet ubicado en www.cesuver.edu.mx</p>
                    <p>QUINTA.- La Institución Evaluadora garantizará en todo momento el derecho que tiene el usuario de acceder a la revisión de los resultados del Examen General de Acreditación en sus ambas etapas.</p>
                    <p>SEXTA.- En todo momento la Institución Evaluadora dignificará el esfuerzo del aspirante, reconociéndole su confianza sin que por ello se demerite la función principal de la valoración de los conocimientos.</p>
                    <p>SÉPTIMA.- Los datos personales que la Institución Evaluadora obtenga de los interesados, serán absolutamente reservados en términos de la Ley Federal de Protección de Datos Personales en Posesión de Particulares.</p>
                    <p>OCTAVA.- En todo momento la Institución Evaluadora dará un seguimiento puntual a cada usuario del estado que guarda su trámite hasta su conclusión, buscando apoyarle en todo momento para facilitar el acceso a su proceso de Acreditación.</p>
                    <p>NOVENA.- El trámite de Titulación ante la Autoridad Educativa, lo realizará en todo momento la Institución Evaluadora CESUVER y el usuario deberá curbrir por su cuenta y de forma individual el costo por la expedición del Título conforme se publique por parte de la SEP.</p>
                    <p>DÉCIMA.- El usuario deberá sujetarse a los plazos que la Autoridad Educativa señale para la emisión del Título Profesional, pudiendo llegar a tardar el trámite entre 10 y 12 meses.</p>
                    <h4>AVISO DE PRIVACIDAD</h4>
                    <p>Univer de Veracruz, A.C. con nombre comercial CESUVER y/o empresas filiales y subsidiarias, con domicilio en Valentín Gómez Farías 722, Col. Ricardo Flores Magón, C.P. 91900, Veracruz, Ver., le avisa que, de conformidad con la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, sus datos personales los cuales pueden incluir datos sensibles, referidos en los documentos requisitados por usted, los que se obtengan por referencias personales y aquellos que generamos, con motivo de los servicios educativos y de evaluación solicitados o contratados por usted con esta Institución y sus centros de aplicación, se tratarán para identificación, operación, administración y para poder proveerle de los servicios mencionados.</p>
                    <p>Sus datos personales no serán transferidos a terceros para fines distintos a los antes mencionados, salvaguardando la privacidad de los mismos.</p>
                    <p>Usted podrá limitar el uso o divulgación de sus datos personales, así como ejercer los derechos de acceso, rectificación, cancelación y oposición, presentando su solicitud por escrito a través del correo electrónico examendeacreditacion@cesuver.edu.mx o por escrito dirigido a Univer de Veracruz A.C. a partir del 1 de enero  de 2018.</p>
                    <p>Las modificaciones que en su caso se hagan al presente aviso, podrá verificarlas en la página electrónica www.cesuver.edu.mx.</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Acepto</button>
                </div>
            </div>
        </div>
    </div>
@endsection
