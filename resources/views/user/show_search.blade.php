@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @include('layouts.alerts')
                <div class="panel panel-default">
                    <div class="panel-heading">Usuarios</div>

                    <div class="panel-body">

                        {!! Form::open(['route' => 'user.search', 'method' => 'post', 'files' => 'true', 'class' => 'form-horizontal']) !!}

                        <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                            <label for="start_date" class="col-md-4 control-label">De</label>

                            <div class="col-md-6">
                                <input id="start_date" type="date" class="form-control" name="start_date" value="{{ old('start_date') }}" autofocus>

                                @if ($errors->has('start_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                            <label for="end_date" class="col-md-4 control-label">Hasta</label>

                            <div class="col-md-6">
                                <input id="descriptive_memory" type="date" class="form-control" name="end_date" value="{{ old('end_date') }}" autofocus>

                                @if ($errors->has('end_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('end_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Buscar
                                </button>

                                <br>

                            </div>

                        </div>
                        {!! Form::close() !!}

                        <table class="table table-hover" id="table">
                            <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Nombre completo</th>
                                <th>Fecha de examen</th>
                                <th>Materia</th>
                                <th>Sede</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    @if(empty($user->evaluation))
                                        <td></td>
                                    @else
                                        <td>{{$user->evaluation->code}}</td>
                                    @endif
                                    <td>{{$user->identities->full_name}}</td>
                                    <td>{{\App\Support\FormatDate::from($user->evaluation->scheduled_date)}}</td>
                                    <td>{{$user->profiles->subject->name}}</td>
                                    <td>{{$user->evaluation->place->name}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
