@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if (session('status'))
                    <div class="alert alert-success">
                        <p>{{ session('status') }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Usuarios</div>

                    <div class="panel-body">

                        <table class="table table-hover" id="table">
                            <thead>
                                <tr>
                                    <th>Folio</th>
                                    <th>Nombre completo</th>
                                    <th>E-mail</th>
                                    <th>Status</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr class="{{is_null($user->identities) ? 'bg-warning': ''}}">
                                    @if($user->evaluations->isEmpty())
                                        <td></td>
                                    @else
                                        <td>{{$user->evaluations->first()->code}}</td>
                                    @endif
                                    @if($user->identities == null)
                                        <td></td>
                                    @else
                                        <td>{{$user->identities->full_name}}</td>
                                    @endif
                                    <td>{{$user->email}}</td>
                                    @if($user->evaluations->isEmpty())
                                        <td></td>
                                    @else
                                        <td>{{$user->evaluations->first()->status}}</td>
                                    @endif
                                    <td class="text-center">
                                        <div class="btn-group">
                                        @if($user->identities != null)
                                            <a href="{!! route('users.edit',['user' => $user->id]) !!}" class="btn btn-sm btn-default" title="Editar Participante" data-toggle="tooltip"><i class="far fa-edit"></i></a>
                                        @else
                                            <a href="{!! route('get.user.profile',['user' => $user->id]) !!}" class="btn btn-sm btn-default" title="Agregar Perfil" data-toggle="tooltip"><i class="far fa-edit"></i></a>
                                        @endif
                                            <a href="{!! route('admin.results',['user' => $user->id]) !!}" class="btn btn-sm btn-default" title="Consultar Resultados" data-toggle="tooltip"><i class="glyphicon glyphicon-eye-open"></i></a>
                                            <a href="{!! route('acceptance', ['id' => $user->id]) !!}" class="btn btn-sm btn-default" title="Programar Evaluación" data-toggle="tooltip"><i class="glyphicon glyphicon-calendar"></i></a>
                                            <a href="{!! route('users.evaluations.index', $user) !!}" class="btn btn-sm btn-default" title="ver Evaluaciones" data-toggle="tooltip"><i class="glyphicon glyphicon-list-alt"></i></a>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
            </div>
        </div>
    </div>
    </div>
@endsection
