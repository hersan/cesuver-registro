@extends('layouts.app')

@section('content')
    <div class="container">
        @if(!$evaluation->practicalScore->isEmpty())
            <div class="panel panel-success">
                <div class="panel-body">
                    <a href="{{route('practical.report',$user->id)}}">Descargar reporte práctico</a>
                </div>
            </div>
        @endif

        {!! Form::open(['route' => ['create.practical', $user->id, $evaluation->id], 'method' => 'post', 'files' => 'true']) !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Agregar resultados de la evaluación: <strong>{{$user->identities->full_name}}</strong></h3>
            </div>
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                <fieldset>
                    <legend>Habilidades, Destrezas y Competencias</legend>

                    <div class="form-group{{ $errors->has('evaluation_type') ? ' has-error' : '' }}">
                        {!! Form::hidden('evaluation_type', 2) !!}
                        @if ($errors->has('evaluation_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('evaluation_type') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="form-group{{ $errors->has('score') ? ' has-error' : '' }}">
                                {!! Form::label('practical_evaluation_date', 'Fecha de Aplicación') !!}
                                {!! Form::date("practical_evaluation_date", old('practical_evaluation_date'), ['class' => 'form-control']) !!}
                                @if ($errors->has('practical_evaluation_date'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('practical_evaluation_date') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    @for($i=1; $i <= 3; $i++)
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <div class="form-group{{ $errors->has('aspects_evaluated.'.$i) ? ' has-error' : '' }}">
                                    {!! Form::label('aspects_evaluated', 'Aspectos evaluados') !!}
                                    {!! Form::text("aspects_evaluated[$i]", old('aspects_type.'.$i), ['class' => 'form-control']) !!}
                                    @if ($errors->has('aspects_evaluated.'.$i))
                                        <span class="help-block">
                                <strong>{{ $errors->first('aspects_evaluated.'.$i) }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('aspects_score.'.$i) ? ' has-error' : '' }}">
                                    {!! Form::label('aspects_score', 'Puntaje obtenido') !!}
                                    {!! Form::text("aspects_score[$i]", old('aspects_score.'.$i), ['class' => 'form-control']) !!}
                                    @if ($errors->has('aspects_score.'.$i))
                                        <span class="help-block">
                                <strong>{{ $errors->first('aspects_score.'.$i) }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endfor
                </fieldset>

                <fieldset>
                    <legend>Comentarios</legend>
                    <div class="col-md-8  col-md-offset-2">
                        <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">
                            {!! Form::label('comments', 'Comentarios') !!}
                            {!! Form::textarea('comments', old('comments'), ['class' => 'form-control']) !!}
                            @if ($errors->has('comments'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('comments') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </fieldset>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2">
                        <button type="submit" class="btn btn-primary">
                            Guardar resultados
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel-footer"></div>
        </div>

        {!! Form::close() !!}

    </div> <!-- End container -->
@endsection