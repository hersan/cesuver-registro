@extends('layouts.app')

@section('content')
    <div class="container">

        {!! Form::open(['route' => ['create.results', $user->id], 'method' => 'post', 'files' => 'true', 'id' => 'result_form']) !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Agregar resultados de la evaluación</h3>
            </div>
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        <p>{{ session('status') }}</p>
                    </div>
                @endif
                <fieldset>
                    <legend>Selecciona la evaluación</legend>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="form-group{{ $errors->has('evaluation_type') ? ' has-error' : '' }}">
                                {!! Form::label("evaluation_type", 'Tipo de evaluación') !!}
                                <label for="evaluation_type">Tipo de evaluación</label>
                                <select name="evaluation_type" id="evaluation_type" v-model="selected">
                                    <option value="1">Evaluación Teórica</option>
                                    <option value="2">Evaluación práctica</option>
                                </select>

                                @if ($errors->has('evaluation_type'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('evaluation_type') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Evaluación Teórica</legend>
                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('score') ? ' has-error' : '' }}">
                                {!! Form::label('score', 'Puntaje obtenido') !!}
                                {!! Form::text("score", old('score'), ['class' => 'form-control']) !!}
                                @if ($errors->has('score'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('score') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Habilidades, Destrezas y Competencias</legend>

                    @for($i=1; $i <= 3; $i++)
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <div class="form-group{{ $errors->has('aspects_evaluated.'.$i) ? ' has-error' : '' }}">
                                    {!! Form::label('aspects_evaluated', 'Aspectos evaluados') !!}
                                    {!! Form::text("aspects_evaluated[$i]", old('aspects_type.'.$i), ['class' => 'form-control']) !!}
                                    @if ($errors->has('aspects_evaluated.'.$i))
                                        <span class="help-block">
                                <strong>{{ $errors->first('aspects_evaluated.'.$i) }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('aspects_score.'.$i) ? ' has-error' : '' }}">
                                    {!! Form::label('aspects_score', 'Puntaje obtenido') !!}
                                    {!! Form::text("aspects_score[$i]", old('aspects_score.'.$i), ['class' => 'form-control']) !!}
                                    @if ($errors->has('aspects_score.'.$i))
                                        <span class="help-block">
                                <strong>{{ $errors->first('aspects_score.'.$i) }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endfor
                </fieldset>

                <fieldset>
                    <legend>Comentarios</legend>
                    <div class="col-md-8  col-md-offset-2">
                        <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">
                            {!! Form::label('comments', 'Comentarios') !!}
                            {!! Form::textarea('comments', old('comments'), ['class' => 'form-control']) !!}
                            @if ($errors->has('comments'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('comments') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </fieldset>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2">
                        <button type="submit" class="btn btn-primary">
                            Registrar
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel-footer"></div>
        </div>

        {!! Form::close() !!}

    </div> <!-- End container -->
@endsection