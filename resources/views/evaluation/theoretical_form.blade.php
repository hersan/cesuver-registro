@extends('layouts.app')

@section('content')
    <div class="container">
        @if(!$evaluation->scores->isEmpty())
            <div class="panel panel-success">
                <div class="panel-body">
                    <a href="{{route('theoretical.report',$user->id)}}">Descargar reporte teórico</a>
                </div>
            </div>
        @endif

        {!! Form::open(['route' => ['create.theoretical', $user->id, $evaluation->id], 'method' => 'post', 'files' => 'true']) !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Agregar resultados de la evaluación teórica: <strong>{{$user->identities->full_name}}</strong></h3>
            </div>
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        <p>{{ session('status') }}</p>
                    </div>
                @endif
                @if (session('danger'))
                    <div class="alert alert-danger">
                        <p>{{ session('danger') }}</p>
                    </div>
                @endif
                <fieldset>
                    <legend>Evaluación Teórica</legend>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="form-group{{ $errors->has('evaluation_type') ? ' has-error' : '' }}">
                                {!! Form::hidden('evaluation_type', 1) !!}
                                @if ($errors->has('evaluation_type'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('evaluation_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('score') ? ' has-error' : '' }}">
                                {!! Form::label('score', 'Puntaje obtenido') !!}
                                {!! Form::text("score", old('score'), ['class' => 'form-control']) !!}
                                @if ($errors->has('score'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('score') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>

                </fieldset> <!-- end fieldset -->

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2">
                        <button type="submit" class="btn btn-primary">
                            Guardar resultados
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel-footer"></div>
        </div>

        {!! Form::close() !!}

    </div> <!-- End container -->
@endsection