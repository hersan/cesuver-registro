@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if (session('status'))
                    <div class="alert alert-success">
                        <p>{{ session('status') }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Evaluaciones de <strong>{{$user->identities->full_name}}</strong></div>

                    <div class="panel-body">

                        <table class="table table-hover" id="table">
                            <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Fecha</th>
                                <th>Institución</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($user->evaluations as $evaluation)
                                <tr>
                                    <td>{{$evaluation->code}}</td>
                                    <td>{{\App\Support\FormatDate::from($evaluation->scheduled_date)}}</td>
                                    <td>{{$evaluation->place->name}}</td>
                                    <td>{{$evaluation->status}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{!! route('admin.results',['user' => $user->id]) !!}" class="btn btn-sm btn-default" title="Consultar Resultados" data-toggle="tooltip"><i class="glyphicon glyphicon-eye-open"></i></a>
                                            <a data-id="{{$user->id}}" href="{!! route('create.theoretical', ['user' => $user->id, 'evaluation' => $evaluation->id]) !!}" class="btn btn-sm btn-default" title="Cargar Teórico" data-toggle="tooltip"><i class="fas fa-file-word fa-lg"></i></a>
                                            <a data-id="{{$user->id}}" href="{!! route('create.practical', ['user' => $user->id, 'evaluation' => $evaluation->id]) !!}" class="btn btn-sm btn-default" title="Cargar Practico" data-toggle="tooltip"><i class="fas fa-file-powerpoint fa-lg"></i></a>
                                            <a id="change-status" data-id="{{$evaluation->id}}" href="#" class="btn btn-sm btn-default" title="Cerrar evaluación" data-toggle="tooltip"><i class="glyphicon glyphicon-stop"></i></a>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("a[title='Cerrar evaluación']").click(function(e) {
            e.preventDefault();

            var evaluation_id = $(this).data('id');
            var button = $(this);

            swal({
                title: '¿Desea cambiar el estatus?',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'No, cancelar!',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonText: 'Enviar',
                confirmButtonClass: 'btn btn-success',
                showLoaderOnConfirm: true,
                preConfirm: function(result) {
                    return new Promise(function(resolve, reject){
                        $.ajax({
                            type: 'post',
                            url: '/status/evaluation',
                            data: {'id': evaluation_id, '_token': '{{ csrf_token() }}'},
                            dataType: 'json',
                            success: function(json) {
                                console.log(json);
                                if (json.status) {
                                    resolve(json.reason)
                                } else {
                                    reject(json.reason);
                                }
                            },

                            error : function(xhr, status) {
                                reject('Error en el servidor');
                            }
                        });
                    })
                },
                allowOutsideClick: false
            }).then(function(reason) {
                console.log(reason);
                if (!(reason.dismiss === swal.DismissReason.cancel)) {
                    swal({
                        type: 'success',
                        'title': reason.value
                    })
                }

            }).catch(function (reason) {
                console.log(reason);
                swal({
                    type: 'error',
                    title: reason
                })
            })
        });
    </script>
@endsection

