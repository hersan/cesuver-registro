@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @if (session('status'))
                    <div class="alert alert-success">
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if (session('warning'))
                    <div class="alert alert-warning">
                        <p>{{ session('warning') }}</p>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Consultar resultados de: <strong>{{$user->identities->full_name}}</strong></div>

                    <div class="panel-body">
                        @if($evaluations == null)
                            <p>No tienes resultados en este momento</p>
                        @else
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Tipo de calificación</th>
                                            <th>Puntaje obtenido</th>
                                            <th>Calificación</th>
                                            <th>Escala de competencias</th>
                                            <th>Obtener reportes</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($evaluations->scores as $score)
                                            <tr>
                                                <td>{{$score->evaluation_type}}</td>
                                                <td>{{$score->score}}</td>
                                                <td>{{number_format($score->calculate_score,1)}}</td>
                                                <td></td>
                                                @if($score->evaluation_type === 'Evaluación Teórica')
                                                    <td>
                                                        <a href="{{route('theoretical.report',$user->id)}}">Descargar reporte teórico</a>
                                                    </td>
                                                @else
                                                    <td>
                                                        <a href="{{route('practical.report',$user->id)}}">Descargar reporte práctico</a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td><strong>Puntaje total obtenido</strong></td>
                                            <td><strong>{{$total_score}}</strong></td>
                                            <td><strong>{{\App\Services\FloatToStringFormatter::formatFloat($total_calculate_score)}}</strong></td>
                                            <td><strong>{{$competitions}}</strong></td>
                                            <td><a href="{{route('global.report',['user' => $user->id])}}">Descargar Reporte</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-5 col-md-offset-3">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th colspan="3">ESCALA DE RESULTADOS</th>
                                        </tr>
                                        <tr>
                                            <th>De</th>
                                            <th>A</th>
                                            <th>Calificación</th>
                                            <th>Resultado</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach(config($scales) as $scala)
                                            <tr>
                                                <td>{{$scala['from']}}</td>
                                                <td>{{$scala['to']}}</td>
                                                <td>{{$scala['mark']}}</td>
                                                <td>{{$scala['value']}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5 col-md-offset-3">
                                    <table class="table table-bordered">
                                        <caption>Comentarios</caption>
                                        <tbody>
                                        <tr>
                                            <td>{{$evaluations->comments}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
