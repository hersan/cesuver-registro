@extends('layouts.pdf')

@section('styles')
    @parent
    <style>
        * {
            font-size: 10px;
            line-height: 2;
        }
    </style>

    <style>
        /**
            Set the margins of the page to 0, so the footer and the header
            can be of the full height and width !
         **/
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin: 4cm 2cm 2cm 2cm;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0.5cm;
            left: 2cm;
            right: 2cm;
            height: 3cm;
        }

        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
        }
    </style>
@endsection

@section('content')

    <header>
        <img class="img-responsive center-block" height="80%"
             width="80%;" src="{{public_path()}}/img/logo.png" alt="">
    </header>

    <div>
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-10 text-justify">
                        <h4 class="text-center">
                            REPORTE INDIVIDUAL DE RESULTADOS
                        </h4>

                        <h4 class="text-center">
                            EXAMEN DE CONOCIMIENTOS APLICADOS (ORAL)
                        </h4>

                        <p>
                            El Centro de Estudios Superiores de Veracruz (CESUVER), expide la presente constancia que muestran los
                            resultados obtenidos en la prueba de tipo oral (Examen de Conocimientos Aplicados), como parte del proceso
                            del Examen General de Acreditación de Licenciatura, que fue aplicado por esta Institución Evaluadora al
                            amparo del Acuerdo Secretarial 286 de fecha 30 de octubre de 2000, y Acuerdo 02/04/17.
                        </p>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                        <table class="table table-condensed">
                            <tr>
                                <td><strong>Folio de solicitud</strong></td>
                                <td>{{$evaluation->code}}</td>
                            </tr>
                            <tr>
                                <td><strong>Nombre</strong></td>
                                <td>{{$user->identities->full_name}}</td>
                            </tr>
                            <tr>
                                <td><strong>Perfil</strong></td>
                                <td>{{$user->profiles->subject->name}}</td>
                            </tr>
                            <tr>
                                <td><strong>Fecha de Aplicación</strong></td>
                                <td>{{
                                    App\Support\FormatDate::from(
                                        empty($evaluation->practical_evaluation_date) ?
                                         $evaluation->practical_date :
                                         $evaluation->practical_evaluation_date
                                    )
                                }}</td>
                            </tr>
                            <tr>
                                <td><strong>Resultado</strong></td>
                                <td>{{$score->score}} de {{$max}}</td>
                            </tr>
                            <tr>
                                <td><strong>Escala de resultados</strong></td>
                                <td>{{$competitions['value']}}</td>
                            </tr>
                            <tr>
                                <td><strong>Calificación</strong></td>
                                <td>{{$score->calculate_score}}</td>
                            </tr>
                        </table>
                    </div>
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-10 text-justify">
                        <p>
                            El presente informe tiene carácter informativo y por ninguna razón representa la opinión de esta Institución
                            Evaluadora sobre la competencia global del aspirante, por lo que es necesario sujetarse al proceso de
                            Evaluación Oral (Examen de Conocimientos Aplicados), pudiendo realizarlo con alguna otra Institución
                            Evaluadora.
                        </p>
                    </div>
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-10 text-justify">
                        <p>
                            De haber obtenido resultados No Satisfactorios en el proceso de Examen Teórico de Conocimientos
                            Disciplinarios, es necesario reiniciar el proceso, debiéndose inscribir nuevamente para poder ser evaluado en
                            las fechas que se asignen para la aplicación del Examen Escrito.
                        </p>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-md-10 text-justify">
                        <br>
                        <p>
                            H. Veracruz, Ver., a {{App\Support\FormatDate::toLocale()}}
                        </p>
                        <p>
                            Coordinación del Examen General de Acreditación de Licenciatura
                        </p>
                        <p>
                            Acuerdo 286
                        </p>
                    </div>
                </div><!-- end row -->

                <footer>
                    <p class="text-center" style="font-size: 8">
                        Valentín Gómez Farías 722, Ricardo Flores Magón, Veracruz,Ver.
                        Telefono 229 9315162, www.cesuver.edu.mx, examendeacreditacion@cesuver.edu.mx
                        Facebook / Cesuver Oficial
                    </p>
                </footer>

            </div>
        </div>
    </div> <!-- End container -->

@show