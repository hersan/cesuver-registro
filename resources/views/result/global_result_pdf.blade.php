@extends('layouts.pdf')

@section('styles')
    @parent
    <style>
        .page-break {
            page-break-after: always;
        }

        * {
            font-size: 12px;
            line-height: 2;
        }
    </style>

    <style>
        /**
            Set the margins of the page to 0, so the footer and the header
            can be of the full height and width !
         **/
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 4cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0.5cm;
            left: 2cm;
            right: 2cm;
            height: 3cm;
        }

        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
        }
    </style>
@endsection

@section('content')

    <header>
        <img class="img-responsive center-block" height="80%"
             width="80%;" src="{{public_path()}}/img/logo.png" alt="logo">
    </header>

    <div>
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-10 text-justify">
                        <h4 class="text-center">
                            DICTAMEN GLOBAL DE RESULTADOS
                        </h4>

                        <h4 class="text-center">
                            EXAMEN GENERAL DE ACREDITACION DE LICENCIATURA ACUERDO 286
                        </h4>

                        <p>El Centro de Estudios Superiores de Veracruz (CESUVER), con fundamento en lo que dispone el Título Tercero del Acuerdo Secretarial 286 de  fecha  30  de  octubre  de 2000, el cual prevé los mecanismos para acreditar conocimientos que correspondan a un cierto nivel educativo o grado escolar, adquiridos en forma autodidacta, a través de la experiencia laboral o con base en el régimen de certificación referido a la formación para el trabajo, hace CONSTAR que:</p>

                        <p align="center"><h4 class="text-center">{{$user->identities->fullname}}</h4></p>

                        <p>Se sujetó a los procesos de evaluación determinados por esta Instancia Evaluadora, de acuerdo con los siguientes datos:</p>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                        <table class="table table-condensed">
                            <tr>
                                <td><strong>Folio de solicitud</strong></td>
                                <td>{{$evaluations->code}}</td>
                                <td><strong>Fecha del examen escrito</strong></td>
                                <td>{{\App\Support\FormatDate::from($evaluations->scheduled_date)}}</td>
                                <td><strong>Fecha del examen oral</strong></td>
                                <td>{{
                                    App\Support\FormatDate::from(
                                        empty($evaluations->practical_evaluation_date) ?
                                         $evaluations->practical_date :
                                         $evaluations->practical_evaluation_date
                                    )
                                }}</td>
                            </tr>
                            <tr>
                                <td><strong>CURP</strong></td>
                                <td>{{$user->identities->curp}}</td>
                                <td><strong>Perfil</strong></td>
                                <td>{{$user->profiles->subject->name}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-10 text-justify">
                        <p>Derivado de lo cual se obtuvieron los siguientes RESULTADOS:</p>
                    </div>
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-5">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Tipo de evaluación</th>
                                <th>Puntaje obtenido</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($evaluations->scores as $score)
                                <tr>
                                    <td>{{$score->evaluation_type}}</td>
                                    <td>{{$score->score}}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td><strong>Puntaje total obtenido</strong></td>
                                <td><strong>{{$total_score}}</strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div><!-- end row -->

                    <div class="col-md-5">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>RESULTADO</td>
                                <td>{{$competitions}}</td>
                            </tr>
                            <tr>
                                <td>Calificación global alcanzada</td>
                                <td>{{\App\Services\FloatToStringFormatter::formatFloat($total_calculate_score)}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="row">

                    </div><!-- end row -->

                    <br>
                    <div class="page-break"></div>

                    <div class="row">
                        <div class="col-md-10 text-justify">

                            <p>En el proceso de evaluación aplicado se midieron conocimientos generales a través del examen escrito, y se midieron las Habilidades, Destrezas y Competencias de las siguientes áreas a través del examen oral o práctico:</p>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>Aspectos evaluados</td>
                                    <td>Puntaje obtenido</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($evaluations->aspects as $aspect)
                                <tr>
                                    <td>{{$aspect->aspect}}</td>
                                    <td>{{$aspect->score}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <p>El Jurado Dictaminador, realizó las siguientes observaciones como parte del resultado del examen oral:</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <table class="table table-bordered">
                                <caption>Comentarios</caption>
                                <tbody>
                                <tr>
                                    <td>{{$evaluations->comments}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th colspan="3">ESCALA DE RESULTADOS</th>
                                </tr>
                                <tr>
                                    <th>De</th>
                                    <th>A</th>
                                    <th>Calificación</th>
                                    <th>Escala</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(config($scale) as $scala)
                                    <tr>
                                        <td>{{$scala['from']}}</td>
                                        <td>{{$scala['to']}}</td>
                                        <td>{{$scala['mark']}}</td>
                                        <td>{{$scala['value']}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> <!-- End container -->

@show