@extends('layouts.pdf')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-off-2">
            <div class="center-block">
                <img class="img-responsive center-block" src="{{asset('img/logo.png')}}" alt="">

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-justify">
                <h2 class="text-center">
                    ACUSE DE RECEPCIÓN DE MEMORIA DESCRIPTIVA DE EXPERIENCIA PROFESIONAL
                </h2>

                <p>Por medio del presente hacemos de su conocimiento que hemos recibido su memoria descriptiva de experiencia profesional</p>

                <p>Agradecemos de antemano la confianza depositada en esta Institución Evaluadora.</p>

            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <table class="table table-condensed">
                    <tr>
                        <td><strong>Folio de solicitud</strong></td>
                        <td>{{$user->evaluations->first()->code}}</td>
                    </tr>
                    <tr>
                        <td><strong>Nombre del solicitante</strong></td>
                        <td>{{$user->identities->full_name}}</td>
                    </tr>
                    <tr>
                        <td><strong>Licenciatura</strong></td>
                        <td>{{$user->profiles->subject->name}}</td>
                    </tr>
                    <tr>
                        <td><strong>Correo Electrónico</strong></td>
                        <td>{{$user->email}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div> <!-- End container -->

    <br>
@show