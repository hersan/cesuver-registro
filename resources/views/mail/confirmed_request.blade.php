@extends('layouts.pdf')

@section('styles')
    @parent
    <style>
        /**
            Set the margins of the page to 0, so the footer and the header
            can be of the full height and width !
         **/
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 4cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0.5cm;
            left: 2cm;
            right: 2cm;
            height: 3cm;
        }

        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
        }
    </style>
@endsection

@section('content')
    <header class="container">
        <img class="img-responsive center-block" height="80%"
            width="80%;" src="{{asset('img/logo.png')}}" alt="">
    </header>

    <div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-justify">
                <h3 class="text-center">
                    ACUSE DE SOLICITUD DE INSCRIPCIÓN
                </h3>

                <p>Por medio del presente hacemos de su conocimiento que hemos recibido su Solicitud de Inscripción al Examen General de Acreditación bajo el Acuerdo Secretarial 286.</p>

                <p>Sus datos y la documentación anexa a su solicitud están siendo revisadas para poder garantizar que cumple con las condiciones necesarias para poder ser evaluado bajo esta modalidad.</p>

                <p>En breve recibirá en su correo electrónico registrado, la información sobre la procedencia de su solicitud a fin de que pueda continuar adecuadamente con el proceso de Acreditación.</p>

                <p>Agradecemos de antemano la confianza depositada en esta Institución Evaluadora.</p>

            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <table class="table table-bordered">
                        {{--<tr>
                            <td>Folio de solicitud</td>
                            <td>{{$user->id}}</td>
                        </tr>--}}
                        <tr>
                            <td><strong>Nombre del solicitante</strong></td>
                            <td>{{$user->identities->full_name}}</td>
                        </tr>
                        <tr>
                            <td><strong>Licenciatura</strong></td>
                            <td>{{$user->profiles->subject->name}}</td>
                        </tr>
                        <tr>
                            <td><strong>Correo Electrónico</strong></td>
                            <td>{{$user->email}}</td>
                        </tr>
                </table>
            </div>
        </div>
    </div> <!-- End container -->

    <br>

@show