@extends('layouts.pdf')

@section('styles')
    <style>
        * {
            font-size: 9px;
            line-height: 2;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <p align="center" style="font-size: 8px;">H. Veracruz., Ver. a {{\App\Support\FormatDate::toLocale()}}</p>

                <p align="center" style="font-size: 8px;">Coordinación del Examen General de Acreditación de Licenciatura
                    Acuerdo 286</p>

                <p align="center" style="font-size: 8px;">EL PRESENTE REPORTE SE EMITE SIN PREJUZGAR DE LA VERACIDAD DE LOS DATOS APORTADOS POR EL ASPIRANTE AL MOMENTO DE SU REGISTRO ANTE ESTA INSTANCIA  EVALUADORA.  DE  NO  CONTAR  CON  UN  ACUERDO  DE  ADMISIÓN EMITIDO POR LA DIRECCION GENERAL DE ACREDITACIÓN, INCORPORACIÓN Y REVALIDACIÓN, NO SERÁ POSIBLE CONTINUAR CON EL PROCESO DE TITULACIÓN</p>

                <p align="center" style="font-size: 8px;">Valentín Gómez Farías 722, Ricardo Flores Magón, Veracruz,Ver.
                    Teléfono 229 9315162, www.cesuver.edu.mx, examendeacreditacion@cesuver.edu.mx
                    Facebook / Cesuver Oficial</p>
            </div>
        </div>
    </div>
@show