@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @include('layouts.alerts');
                <div class="panel panel-default">
                    <div class="panel-heading">Nueva Licenciatura</div>

                    <div class="panel-body" id="app">
                        {!! Form::model($subject, ['route' => ['subjects.update', $subject->id], 'method' => 'put', 'files' => 'true']) !!}

                        <fieldset>
                            <legend>Institución</legend>

                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name">Nombre</label>
                                        <input id="name" type="text" class="form-control" name="name"
                                               value="{{ old('name', $subject->name) }}" autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div> <!-- END ROW -->

                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group{{ $errors->has('scale') ? ' has-error' : '' }}">
                                        <label for="scale">Escala</label>
                                        <select name="scale" class="form-control">
                                                <option value="1" {{old('scale',$subject->scale) == 1 ? 'selected': ''}}>Escala 1</option>
                                                <option value="2" {{old('scale',$subject->scale) == 2 ? 'selected': ''}}>Escala 2</option>
                                        </select>

                                        @if ($errors->has('scale'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div> <!-- END ROW -->

                        </fieldset> <!-- end identity -->

                        <div class="form-group">
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-primary">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
