@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Licenciaturas</strong></div>
                    <div class="panel-body">
                        @include('layouts.alerts')
                        <table class="table table-hover" id="subject">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($subjects as $subjects)
                                <tr>
                                    <td>{{$subjects->name}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['subjects.destroy', $subjects->id]]) !!}
                                            <a data-id="{{$subjects->id}}" href="{!! route('subjects.create') !!}" class="btn btn-sm btn-default" title="Crear Licenciatura" data-toggle="tooltip"><i class="fas fa-plus-circle"></i></a>
                                            <a href="{!! route('subjects.edit',['user' => $subjects->id]) !!}" class="btn btn-sm btn-default" title="Editar Licenciatura" data-toggle="tooltip"><i class="far fa-edit"></i></a>
                                            <button type="submit" class="btn btn-sm btn-danger" title="Borrar Licenciatura" data-toggle="tooltip"><i class="glyphicon glyphicon-trash"></i></button>
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection