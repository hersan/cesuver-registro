@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Fechas de aplicación</strong></div>
                    <div class="panel-body">
                        @include('layouts.alerts')
                        <table class="table table-hover" id="schedule">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Tipo de examen</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($schedules as $schedule)
                                <tr>
                                    <td>{{$schedule->date_at}}</td>
                                    <td>{{$schedule->type == 'teorico' ? 'Teórico': 'Práctico'}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['schedules.destroy', $schedule->id]]) !!}
                                            <a data-id="{{$schedule->id}}" href="{!! route('schedules.create') !!}" class="btn btn-sm btn-default" title="Crear Sede" data-toggle="tooltip"><i class="fas fa-plus-circle"></i></a>
                                            <a href="{!! route('schedules.edit',['user' => $schedule->id]) !!}" class="btn btn-sm btn-default" title="Editar Sede" data-toggle="tooltip"><i class="far fa-edit"></i></a>
                                            <button type="submit" class="btn btn-sm btn-danger" title="Borrar Sede" data-toggle="tooltip"><i class="glyphicon glyphicon-trash"></i></button>
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection