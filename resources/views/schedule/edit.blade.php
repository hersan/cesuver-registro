@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @include('layouts.alerts');
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Fecha</div>

                    <div class="panel-body" id="app">
                        {!! Form::model($schedule, ['route' => ['schedules.update', $schedule->id], 'method' => 'put', 'files' => 'true']) !!}

                        <fieldset>
                            <legend>Fecha</legend>

                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group{{ $errors->has('date_at') ? ' has-error' : '' }}">
                                        <label for="name">Fecha</label>
                                        <input id="name" type="date" class="form-control" name="date_at" value="{{ old('date_at', $schedule->date_at) }}"  autofocus>

                                        @if ($errors->has('date_at'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('date_at') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div> <!-- END ROW -->

                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                        <label for="type" class="control-label">Tipo</label>
                                        {!! Form::select('type', ['teorico' => 'Teórico', 'practico' => 'Práctico'], old('type'), ['class' => 'form-control']) !!}

                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div> <!-- END ROW -->

                        </fieldset> <!-- end identity -->

                        <div class="form-group">
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-primary">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
