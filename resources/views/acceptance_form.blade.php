@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @include('layouts.alerts')
                <div class="panel panel-default">
                    <div class="panel-heading">Generar documento de aceptación: <strong>{{$user->identities->full_name}}</strong></div>

                    <div class="panel-body">
                        {!! Form::open(['route' => ['acceptance', $user->id], 'method' => 'post', 'files' => 'true', 'class' => 'form-horizontal']) !!}

                        <div class="col-md-5 col-md-offset-4">
                            <div class="form-group{{ $errors->has('scheduled_date') ? ' has-error' : '' }}">
                                <label for="scheduled_date" class="control-label">Programar fecha (Examen Teórico)</label>
                                {!! Form::select('scheduled_date', $theoretical_dates, old('scheduled_date'), ['class' => 'form-control']) !!}

                                @if ($errors->has('scheduled_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('scheduled_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-5 col-md-offset-4">
                            <div class="form-group{{ $errors->has('practical_date') ? ' has-error' : '' }}">
                                <label for="practical_date" class="control-label">Programar fecha (Examen Práctico)</label>
                                {!! Form::select('practical_date', $practical_dates, old('practical_date'), ['class' => 'form-control']) !!}

                                @if ($errors->has('practical_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('practical_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-5 col-md-offset-4">
                            <div class="form-group{{ $errors->has('place') ? ' has-error' : '' }}">
                                <label for="place" class="control-label">Sede de Examen</label>
                                {!! Form::select('place', $places, old('place'), ['class' => 'form-control']) !!}

                                @if ($errors->has('place'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('place') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection