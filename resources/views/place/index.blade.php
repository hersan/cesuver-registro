@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Instituciones</strong></div>
                    <div class="panel-body">
                        @include('layouts.alerts')
                        <table class="table table-hover" id="sede">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($places as $place)
                                <tr>
                                    <td>{{$place->name}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['places.destroy', $place->id]]) !!}
                                            <a data-id="{{$place->id}}" href="{!! route('places.create') !!}" class="btn btn-sm btn-default" title="Crear Sede" data-toggle="tooltip"><i class="fas fa-plus-circle"></i></a>
                                            <a href="{!! route('places.edit',['user' => $place->id]) !!}" class="btn btn-sm btn-default" title="Editar Sede" data-toggle="tooltip"><i class="far fa-edit"></i></a>
                                            <button type="submit" class="btn btn-sm btn-danger" title="Borrar Sede" data-toggle="tooltip"><i class="glyphicon glyphicon-trash"></i></button>
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection