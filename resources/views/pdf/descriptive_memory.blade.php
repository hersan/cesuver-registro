@extends('layouts.pdf')

@section('styles')
    @parent
    <style>
        /**
            Set the margins of the page to 0, so the footer and the header
            can be of the full height and width !
         **/
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 4cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0.5cm;
            left: 2cm;
            right: 2cm;
            height: 3cm;
        }

        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
        }
    </style>
@endsection

@section('content')

    <header class="container">
        <img class="img-responsive center-block" height="80%"
             width="80%;" src="{{public_path()}}/img/logo.png" alt="">
    </header>

    <div class="row">
        <div class="col-md-8 text-justify">
            <h3 class="text-center">
                ACUSE DE RECEPCIÓN DE MEMORIA DESCRIPTIVA DE EXPERIENCIA PROFESIONAL
            </h3>

            <p>Por medio del presente hacemos de su conocimiento que hemos recibido su memoria descriptiva de experiencia profesional</p>

            <p>Agradecemos de antemano la confianza depositada en esta Institución Evaluadora.</p>

        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <table class="table table-condensed">
                <tr>
                    <td><strong>Folio de solicitud</strong></td>
                    <td>{{$user->evaluations->first()->code}}</td>
                </tr>
                <tr>
                    <td><strong>Nombre del solicitante</strong></td>
                    <td>{{$user->identities->full_name}}</td>
                </tr>
                <tr>
                    <td><strong>Licenciatura</strong></td>
                    <td>{{$user->profiles->subject->name}}</td>
                </tr>
                <tr>
                    <td><strong>Correo Electrónico</strong></td>
                    <td>{{$user->email}}</td>
                </tr>
            </table>
        </div>
    </div><!-- End container -->

    <br>
@show