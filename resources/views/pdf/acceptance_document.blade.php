@extends('layouts.pdf')

@section('styles')
    @parent

    <style>
        /**
            Set the margins of the page to 0, so the footer and the header
            can be of the full height and width !
         **/
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 3cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 3cm;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0.5cm;
            left: 2cm;
            right: 2cm;
            height: 3cm;
        }

        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 2cm;
            right: 2cm;
            height: 3cm;
        }

    </style>

@endsection

@section('content')

    <header class="container" style="font-size: 11px">
        <img class="img-responsive center-block" height="80%"
             width="80%;" src="{{public_path()}}/img/logo.png" alt="logo">
    </header>

    <div class="text-justify" style="font-size: 11px">
        <h3 class="text-center">
            CARTA DE ACEPTACIÓN
        </h3>

        <p>En relación a su solicitud de inscripción al proceso de evaluación para la presentación del Examen General de Acreditación, en base al Acuerdo Secretarial 286, se expide la presente:</p>

        <table class="table table-condensed">
            <tr>
                <td><strong>Folio de solicitud</strong></td>
                <td>{{$user->evaluations->last()->code}}</td>
            </tr>
            <tr>
                <td><strong>Nombre del solicitante</strong></td>
                <td>{{$user->identities->full_name}}</td>
            </tr>
            <tr>
                <td><strong>CURP</strong></td>
                <td>{{$user->identities->curp}}</td>
            </tr>
            <tr>
                <td><strong>Licenciatura</strong></td>
                <td>{{$user->profiles->subject->name}}</td>
            </tr>
            <tr>
                <td><strong>Fecha programada</strong></td>
                <td>{{\App\Support\FormatDate::from($user->evaluations->last()->scheduled_date)}}</td>
            </tr>
            <tr>
                <td><strong>Sede</strong></td>
                <td>{{$user->evaluations->last()->place->name}}</td>
            </tr>
            <tr>
                <td><strong>Correo Electrónico</strong></td>
                <td>{{$user->email}}</td>
            </tr>
            <tr>
                <td><strong>Teléfono</strong></td>
                <td>{{$user->telephones()->first()->number}}</td>
            </tr>
        </table>

        <p>El solicitante deberá presentarse en la fecha programa en las instalaciones de la SEDE que le haya sido designada, de forma puntual para presentar los exámenes de acreditación necesarios para poder evaluar el nivel de conocimientos, habilidades y destrezas en la licenciatura que pretende acreditar. CESUVER será el encargado de aplicar los exámenes correspondientes.</p>

        <p>Posterior al proceso de evaluación, si existieren resultados favorables, el solicitante recibirá las constancias de acreditación para poder iniciar el trámite de titulación ante la Dirección General de Acreditación, Incorporación y Revalidación de la SEP.</p>

        <p>Las guías de estudio son gratuitas y se encuentran disponibles en <a href="http://www.cesuver.edu.mx">www.cesuver.edu.mx</a></p>

        <p>CESUVER no avala, reconoce o respalda a ningún centro de capacitación no autorizado, manifestándose ajeno a los procesos de preparación que estos realicen para la presentación de los exámenes.</p>

        <p>El proceso de evaluación consta de dos etapas, una escrita y otra oral, las cuales deben ser aprobadas por el aspirante para poder ser sujeto de tramitar su título profesional.</p>

        <p><strong>INFORMACIÓN IMPORTANTE.</strong></p>

        <p><strong>En caso de haber aprobado el primer examen con alguna otra Institución Evaluadora, el aspirante deberá hacerlo de conocimiento, con oportunidad, al Centro de Estudios Superiores de Veracruz.</strong></p>

        <p><strong>Al presentarse, el aspirante deberá exhibir el presente, junto con una Identificación Oficial con fotografía, para poder acceder al recinto en el que se aplicará el Examen.</strong></p>

        <br>

        <table class="table-condensed" style="margin-left: auto; margin-right: auto;">
            <tr>
                <td align="center"><p>H. Veracruz, Ver., a {{\App\Support\FormatDate::toLocale()}}</p></td>
            </tr>
            <tr>
                <td align="center">_____________________________________</td>
            </tr>
            <tr>
                <td align="center">Firma del solicitante</td>
            </tr>
        </table>

    </div>

    <footer>
        <p class="text-center"><strong>AVISO DE PRIVACIDAD</strong></p>
        <p align="justify" style="font-size: 8px;">Univer de Veracruz, A.C. con nombre comercial CESUVER y/o empresas filiales y subsidiarias, con domicilio en Valentín Gómez Farías 722, Col. Ricardo Flores Magón, C.P. 91900, Veracruz, Ver., le avisa que, de conformidad con la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, sus datos personales los cuales pueden incluir datos sensibles, referidos en los documentos requisitados por usted, los que se obtengan por referencias personales y aquellos que generamos, con motivo de los servicios educativos y de evaluación solicitados o contratados por usted con esta Institución y sus centros de aplicación, se tratarán para identificación, operación, administración y para poder proveerle de los servicios mencionados.</p>
        <p align="justify" style="font-size: 8px;">Sus datos personales no serán transferidos a terceros para fines distintos a los antes mencionados, salvaguardando la privacidad de los mismos.</p>
        <p align="justify" style="font-size: 8px;">Usted podrá limitar el uso o divulgación de sus datos personales, así como ejercer los derechos de acceso, rectificación, cancelación y oposición, presentando su solicitud por escrito a través del correo electrónico examendeacreditacion@cesuver.edu.mx o por escrito dirigido a Univer de Veracruz A.C. a partir del 1 de enero  de 2018.</p>
        <p align="justify" style="font-size: 8px;">Las modificaciones que en su caso se hagan al presente aviso, podrá verificarlas en la página electrónica www.cesuver.edu.mx.</p>
    </footer>
@show