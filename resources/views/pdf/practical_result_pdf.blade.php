@extends('layouts.pdf')

@section('styles')
    <style>
        * {
            font-size: 12px;
            line-height: 2;
        }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-off-2">
            <div class="center-block">
                <img class="img-responsive center-block" src="{{public_path()}}/img/logo.png" alt="">
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-10 col-md-offset-2 text-justify">
                        <h4 class="text-center">
                            REPORTE INDIVIDUAL DE RESULTADOS
                        </h4>

                        <h4 class="text-center">
                            EXAMEN DE CONOCIMIENTOS APLICADOS (ORAL)
                        </h4>

                        <p>
                            El Centro de Estudios Superiores de Veracruz (CESUVER), expide la presente constancia que muestran los
                            resultados obtenidos en la prueba de tipo oral (Examen de Conocimientos Aplicados), como parte del proceso
                            del Examen General de Acreditación de Licenciatura, que fue aplicado por esta Institución Evaluadora al
                            amparo del Acuerdo Secretarial 286 de fecha 30 de octubre de 2000, y Acuerdo 02/04/17.
                        </p>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <table class="table table-condensed">
                            <tr>
                                <td><strong>Folio de solicitud</strong></td>
                                <td>{{$evaluation->code}}</td>
                            </tr>
                            <tr>
                                <td><strong>Nombre</strong></td>
                                <td>{{$user->identities->full_name}}</td>
                            </tr>
                            <tr>
                                <td><strong>Perfil</strong></td>
                                <td>{{$user->profiles->subject->name}}</td>
                            </tr>
                            <tr>
                                <td><strong>Fecha de Aplicación</strong></td>
                                <td>{{App\Support\FormatDate::from($evaluation->scheduled_date)}}</td>
                            </tr>
                            <tr>
                                <td><strong>Resultado</strong></td>
                                <td>{{$score->score}} de {{config('evaluation.score')}}</td>
                            </tr>
                            <tr>
                                <td><strong>Escala de resultados</strong></td>
                                <td>{{$competitions}}</td>
                            </tr>
                            <tr>
                                <td><strong>Calificación</strong></td>
                                <td>{{$score->calculate_score}}</td>
                            </tr>
                            </tr>
                        </table>
                    </div>
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-10 col-md-offset-2 text-justify">
                        <p>
                            El presente informe tiene carácter informativo y por ninguna razón representa la opinión de esta Institución
                            Evaluadora sobre la competencia global del aspirante, por lo que es necesario sujetarse al proceso de
                            Evaluación Oral (Examen de Conocimientos Aplicados), pudiendo realizarlo con alguna otra Institución
                            Evaluadora.
                        </p>
                    </div>
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-10 col-md-offset-2 text-justify">
                        <p>
                            De haber obtenido resultados No Satisfactorios en el proceso de Examen Teórico de Conocimientos
                            Disciplinarios, es necesario reiniciar el proceso, debiéndose inscribir nuevamente para poder ser evaluado en
                            las fechas que se asignen para la aplicación del Examen Escrito.
                        </p>
                    </div>
                </div><!-- end row -->

            </div>
        </div>
    </div> <!-- End container -->

@show