@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (session('status'))
                    <div class="alert alert-success">
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if (session('warning'))
                    <div class="alert alert-warning">
                        <p>{{ session('warning') }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Agregar memoria descriptiva</div>

                    <div class="panel-body">
                        {!! Form::open(['route' => 'register.memory', 'method' => 'post', 'files' => 'true', 'class' => 'form-horizontal']) !!}

                        <div class="form-group{{ $errors->has('descriptive_memory') ? ' has-error' : '' }}">
                            <label for="descriptive_memory" class="col-md-4 control-label"></label>

                            <div class="col-md-6">
                                <input id="descriptive_memory" type="file" class="form-control" name="descriptive_memory" value="{{ old('descriptive_memory') }}" autofocus>

                                @if ($errors->has('descriptive_memory'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('descriptive_memory') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Guardar memoria
                                </button>

                                <br>

                            </div>

                        </div>
                        <p align="center"><a href="{{url('user/download')}}">Manual para presentar la Memoria Descriptiva de Experiencia Laboral</a></p>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection