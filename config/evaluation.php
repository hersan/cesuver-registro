<?php

return [

    'scale' => [
        [
            'from' => 0,
            'to' => 419,
            'mark' => 'De 0 a 6.9',
            'value' => 'INSUFICIENTE'
        ],
        [
            'from' => 420,
            'to' => 479,
            'mark' => 'De 7.0 a 7.9',
            'value' => 'SUFICIENTE'
        ],
        [
            'from' => 480,
            'to' => 539,
            'mark' => 'De 8.0 a 8.9',
            'value' => 'BUENO'
        ],
        [
            'from' => 540,
            'to' => 600,
            'mark' => '9.0 a 10.00',
            'value' => 'EXCELENTE  '
        ],
    ],

    'theoretical_scale' => [
        [
            'from' => 0,
            'to' => 209,
            'value' => 'INSUFICIENTE'
        ],
        [
            'from' => 210,
            'to' => 240,
            'value' => 'SUFICIENTE'
        ],
        [
            'from' => 241,
            'to' => 270,
            'value' => 'BUENO'
        ],
        [
            'from' => 271,
            'to' => 300,
            'value' => 'EXCELENTE'
        ],
    ],

    'score' => 300,

    'new_theoretical_scale' => [
        [
            'from' => 0,
            'to' => 153,
            'mark' => 'De 0 a 6.9',
            'value' => 'NO APROBADO'
        ],
        [
            'from' => 154,
            'to' => 175,
            'mark' => 'De 7.0 a 7.9',
            'value' => 'APROBADO'
        ],
        [
            'from' => 176,
            'to' => 197,
            'mark' => 'De 8.0 a 8.9',
            'value' => 'APROBADO'
        ],
        [
            'from' => 198,
            'to' => 220,
            'mark' => '9.0 a 10.00',
            'value' => 'APROBADO'
        ],
    ],

    'new_practical_scale' => [
        [
            'from' => 0,
            'to' => 220,
            'mark' => 'De 0 a 6.9',
            'value' => 'NO APROBADO'
        ],
        [
            'from' => 221,
            'to' => 251,
            'mark' => 'De 7.0 a 7.9',
            'value' => 'APROBADO'
        ],
        [
            'from' => 252,
            'to' => 283,
            'mark' => 'De 8.0 a 8.9',
            'value' => 'APROBADO'
        ],
        [
            'from' => 284,
            'to' => 315,
            'mark' => '9.0 a 10.00',
            'value' => 'APROBADO'
        ],
    ],

    'new_global_scale' => [
        [
            'from' => 0,
            'to' => 374,
            'mark' => 'De 0 a 6.9',
            'value' => 'NO APROBADO'
        ],
        [
            'from' => 375,
            'to' => 427,
            'mark' => 'De 7.0 a 7.9',
            'value' => 'APROBADO'
        ],
        [
            'from' => 428,
            'to' => 481,
            'mark' => 'De 8.0 a 8.9',
            'value' => 'APROBADO'
        ],
        [
            'from' => 482,
            'to' => 535,
            'mark' => '9.0 a 10.00',
            'value' => 'APROBADO'
        ],
    ],

    'max_new_theoretical_scale' => 220,

    'max_new_practical_scale' => 315
];