<?php

return [

    'identity' => [
        'fields' => [
            'first_name',
            'second_name',
            'last_name',
            'sex',
            'birthday',
            'curp',
        ],

        'files' => [
            'curp_file',
            'identity_card'
        ]
    ],

    'address' => [
        'fields' => [
            'street',
            'neighborhood',
            'postal_code',
            'state',
            'city',
            'references',
        ],

        'files' => [],
    ],

    'telephone' => [
        'fields' => [
            'number',
            'telephone_type',
        ],

        'files' => [],
    ],

    'profile' => [
        'fields' => [
            'degree_in',
        ],

        'files' => [
            'proof_of_sufficiency_file',
            'curriculum',
            'portfolio_of_evidence',
        ]
    ]


];