<?php

return [
    'production' => [
        ['name' => UserSeed::class,        'callable' => false],
        ['name' => GeneratorSeed::class,   'callable' => false],
        ['name' => PlaceSeed::class,       'callable' => false],
        ['name' => SubjectSeed::class,     'callable' => false],
        ['name' => ScheduleSeed::class,    'callable' => true],
    ],

    'local' => [
        ['name' => UserSeed::class,        'callable' => true],
        ['name' => EvaluationSeed::class,  'callable' => true],
        ['name' => GeneratorSeed::class,   'callable' => true],
        ['name' => IdentitySeed::class,    'callable' => true],
        ['name' => PlaceSeed::class,       'callable' => true],
        ['name' => ProfileSeed::class,     'callable' => true],
        ['name' => SubjectSeed::class,     'callable' => true],
        ['name' => TelephoneSeed::class,   'callable' => true],
        ['name' => AddressSeed::class,     'callable' => true],
        ['name' => ScheduleSeed::class,    'callable' => false],
    ],
];